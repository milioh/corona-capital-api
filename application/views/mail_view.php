<html>
	<head>
		<meta charset="UTF-8">
		<title><?=$subject?></title>
		<meta name="robots" content="noindex,nofollow">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Corona Capital">
		<meta name="author" content="@sclbits">
	    
	    <!-- Le styles -->
		<style type="text/css">
		body {background-color:yellow;}
		p {color:blue;}
		</style>
		
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="span12">
					<img src="<?=base_url()?>bootstrap/images/black.svg" alt="SCLBITS" title="SCLBITS" style="margin-top:20px;margin-bottom:20px; width:140px;" />
					<?php $title = explode('at', $subject); ?>
					<div class="line">
						<div class="action">
							<h2><?=trim($title['0'])?></h2>
							<div class="space"></div>
							<h4><?=date('l, d F')?></h4>
						</div>
					</div>
					<h4>Hello <?=$to_name?></h4>
					<br />
					<?=$body?>
					<center style="margin-top:40px;">
						<a href="<?=$redirect?>" class="btn btn-large btn-inverse" target="_blank">Access</a>
					</center> 
					<div class="pull-left" style="margin-top:40px;">
						<p><?=date('Y')?> © SCLBITS.</p>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>