<?php

include_once('TwitterAPIExchange.php');

class TwitterAPI
{
	public function getFriends($screen_name, $token, $token_secret)
	{
		$settings = array(
			'oauth_access_token' => $token,
			'oauth_access_token_secret' => $token_secret,
			'consumer_key' => 'GD5ScGiYLngCM12oHXww',
			'consumer_secret' => 'KK84pMc3pAxnJzWRxCx1ZXt2HfD23VEorBcTWkwXA'
		);
		
		$twitter = new TwitterAPIExchange($settings);
		
		$getField = '?cursor=-1&screen_name='.$screen_name.'&count=5000';
		
		$response = $twitter->setGetfield($getField)
			->buildOauth('https://api.twitter.com/1.1/friends/ids.json', 'GET')
			->performRequest();
		
		$json = json_decode($response);
		
		return $json;
	}
	
	public function getLookup($ids, $token, $token_secret)
	{
		$settings = array(
			'oauth_access_token' => $token,
			'oauth_access_token_secret' => $token_secret,
			'consumer_key' => 'GD5ScGiYLngCM12oHXww',
			'consumer_secret' => 'KK84pMc3pAxnJzWRxCx1ZXt2HfD23VEorBcTWkwXA'
		);
		
		$twitter = new TwitterAPIExchange($settings);
		
		$getField = '?user_id='.$ids;
		
		$response = $twitter->setGetfield($getField)
			->buildOauth('https://api.twitter.com/1.1/users/lookup.json', 'GET')
			->performRequest();
		
		$json = json_decode($response);
		
		return $json;
	}
}