<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Social extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//Respuesta de Error
		$array = array(
			'status' => (int)0,
			'msg' => (string)'JSON not readable.'
		);
			
		//Imprimimos el Arreglo
		$this->call->printJSON($array);
		$output = TRUE;
	}
	
	public function facebook()
	{
		//Leemos el ID Mandado
		$id = $this->uri->segment(3,'');
		
		//Verificamos
		if ($id != '')
		{
			//Consultamos que el usuario exista en la app
			$this->db->where('user', $id);
			$this->db->where('status', 1);
			$usuario = $this->db->get('user');
			
			//Verificamos
			if ($usuario->num_rows() > 0)
			{
				//Configuramos Facebook
				$facebook = new Facebook(array(
				  'appId'  => '499887483437868',
				  'secret' => '83b3ab2573b201beab8a89f1e421062b',
				));
				
				//Leemos el Objeto
				$usuario_row = $usuario->row();
				$app_secret = '83b3ab2573b201beab8a89f1e421062b';
				$app_id = '499887483437868';
				$my_url = base_url().'social/facebook/'.$id;
				$access_token = '';
				
				//Leemos el Code
				$code = (isset($_GET['code'])) ? (string)trim($_GET['code']) : '';
				
				if ($code!='')
				{
					$token_url="https://graph.facebook.com/oauth/access_token?client_id="
				      . $app_id . "&redirect_uri=" . urlencode($my_url) 
				      . "&client_secret=" . $app_secret 
				      . "&code=" . $code . "&display=popup";
				    $response = file_get_contents($token_url);
				    $params = null;
				    parse_str($response, $params);
				    $access_token = $params['access_token'];
				}
				
				//Verificamos que no haya error
				if (!isset($_GET['error']))
				{	
					if ($access_token) 
					{
						try 
						{
							if ($access_token!='')
							{	
								//Guardamos el Token
								$param_token = array('access_token' => $access_token);			
								$user_profile = $facebook->api('/me','GET',$param_token);
								
								//Valores de Facebook
								$id = (isset($user_profile['id'])) ? (string)trim($user_profile['id']) : '';
								$name = (isset($user_profile['name'])) ? (string)trim($user_profile['name']) : '';
								$first_name = (isset($user_profile['first_name'])) ? (string)trim($user_profile['first_name']) : '';
								$last_name = (isset($user_profile['last_name'])) ? (string)trim($user_profile['last_name']) : '';
								$link = (isset($user_profile['link'])) ? (string)trim($user_profile['link']) : '';
								$username = (isset($user_profile['username'])) ? (string)trim($user_profile['username']) : '';
								$bio = (isset($user_profile['bio'])) ? (string)trim($user_profile['bio']) : '';
								$gender = (isset($user_profile['gender'])) ? (string)trim($user_profile['gender']) : '';
								if ($gender == 'male') { $gender = 'Masculino'; }
								else if ($gender == 'female') { $gender = 'Femenino'; }
								else { $gender = 'Desconocido'; }
								$email = (isset($user_profile['email'])) ? (string)trim($user_profile['email']) : '';
								
								//Leemos el Token Largo
								$tokenLongArray = $this->curl->simple_get('https://graph.facebook.com/oauth/access_token?client_id=499887483437868&client_secret=83b3ab2573b201beab8a89f1e421062b&grant_type=fb_exchange_token&fb_exchange_token='.$access_token);
								$tokenLongArray = explode('&', $tokenLongArray);
								$tokenLong = $tokenLongArray[0];
								$tokenLong = explode('=', $tokenLong);
								$finalToken = $tokenLong[1];
								
								//Leemos los Permisos
								$permisos_json = $this->curl->simple_get('https://graph.facebook.com/'.$id.'/permissions?access_token='.$finalToken);
								$permisos_array = json_decode($permisos_json, true);
								$permisos = $permisos_array['data'][0];
								$publish = (isset($permisos['publish_actions'])) ? (int)$permisos['publish_actions'] : 0;
								
								//Setup Mexico
								date_default_timezone_set('America/Mexico_City');
								
								//Consultamos los Registros del Usuario
								$this->db->where('facebook_id',(string)$id);
								$facebooks = $this->db->get('facebook');
								
								//Bandera 
								$bandera = 0;
								
								//Procesamos los Registros
								if ($facebooks->num_rows() > 0)
								{
									//Leemos el Objeto
									$facebook_row = $facebooks->row();
									$bandera = $facebook_row->iduser;
								}
								
								//Verificamos si es Nuevo
								if ($bandera == 0)
								{
									//Generamos el Arreglo para Insertar
									$data = array(
										'fullname' => $name,
										'first_name' => $first_name,
										'last_name' => $last_name,
										'link' => $link,
										'username' => $username,
										'bio' => $bio,
										'gender' => $gender,
										'email' => $email,
										'facebook_id' => $id,
										'access_token' => $access_token,
										'access_token_long' => $finalToken,
										'publish' => $publish,
										'iduser' => $usuario_row->iduser,
										'user' => $usuario_row->user,
										'status' => (int)1
									);
									$this->db->insert('facebook',$data);
									
									//Leemos el id del registro generado
									$this->db->where('facebook_id', $id);
									$this->db->order_by('iduser', 'desc');
									$query = $this->db->get('facebook');
									$query_row = $query->row();
								}
								else
								{
									//Actualizamos los Tokens
									$data = array(
										'fullname' => $name,
										'first_name' => $first_name,
										'last_name' => $last_name,
										'link' => $link,
										'username' => $username,
										'bio' => $bio,
										'gender' => $gender,
										'email' => $email,
										'access_token' => $access_token,
										'access_token_long' => $finalToken,
										'publish' => $publish,
										'iduser' => $usuario_row->iduser,
										'user' => $usuario_row->user,
										'status' => (int)1
									);
									$this->db->where('facebook_id', $id);
									$this->db->update('facebook', $data);
									
									//Leemos el id del registro generado
									$this->db->where('facebook_id', $id);
									$query = $this->db->get('facebook');
									$query_row = $query->row();
								}
								
								//Redirect Error
								redirect(base_url().'success');
							}
							else
							{
								//Generamos la URL de Login
								$loginUrl = $facebook->getLoginUrl(array(
							    	'scope' => 'email,publish_actions'
								));
								
								//Generamos el Login
								redirect($loginUrl);
							}
						} 
						catch (FacebookApiException $e) 
						{
						    //Generamos la URL de Login
							$loginUrl = $facebook->getLoginUrl(array(
						    	'scope' => 'email,publish_actions'
							));
							
							//Generamos el Login
							redirect($loginUrl);
						}
					}
					else
					{
						//Generamos la URL de Login
						$loginUrl = $facebook->getLoginUrl(array(
					    	'scope' => 'email,publish_actions'
						));
						
						//Generamos el Login
						redirect($loginUrl);
					}
					
				}
				else
				{
					//Mensaje de Error
					$this->session->set_userdata('error', 'Permissions Denied.');
					
					//Redirect Error
					redirect(base_url().'error');
				}
			}
			else
			{
				//Mensaje de Error
				$this->session->set_userdata('error', 'This user does not exists or its disabled.');
				
				//Redirect Error
				redirect(base_url().'error');
			}
		}
		else
		{
			//Mensaje de Error
			$this->session->set_userdata('error', 'You need a user to connect social networks.');
			
			//Redirect Error
			redirect(base_url().'error');
		}
	}
	
	public function fb_logout()
	{
		//Configuramos Facebook
		$facebook = new Facebook(array(
		  'appId'  => '499887483437868',
		  'secret' => '83b3ab2573b201beab8a89f1e421062b',
		));
		
		//Mensaje de Error
		$this->session->set_userdata('error', 'Logout.');
			
		//Generamos la URL de Login
		$logoutUrl = $facebook->getLogoutUrl(array(
	    	'next' => base_url().'error'
		));
		
		//Generamos el Login
		redirect($logoutUrl);
	}
	
	public function twitter()
	{
		//Leemos el ID Mandado
		$id = $this->uri->segment(3,'');
		
		//Verificamos
		if ($id != '')
		{
			//Consultamos que el usuario exista en la app
			$this->db->where('user', $id);
			$this->db->where('status', 1);
			$usuario = $this->db->get('user');
			
			//Verificamos
			if ($usuario->num_rows() > 0)
			{
				//Leemos el Objeto
				$usuario_row = $usuario->row();
				
				//Guardamos en Session el Usuario
				$this->session->set_userdata('tw_user', $usuario_row);
				
				//Mandamos a oAuth
				redirect('http://192.241.204.161/api/twtest/redirect');
			}
			else
			{
				//Mensaje de Error
				$this->session->set_userdata('error', 'This user does not exists or its disabled.');
				
				//Redirect Error
				redirect('http://192.241.204.161/api/error');
			}
		}
		else
		{
			//Mensaje de Error
			$this->session->set_userdata('error', 'You need a user to connect social networks.');
			
			//Redirect Error
			redirect('http://192.241.204.161/api/error');
		}
	}
	
	public function tw_out_login()
	{
		$user = $this->session->userdata('tw_user')->user;
		$this->session->sess_destroy();
		redirect('http://192.241.204.161/api/social/twitter/'.$user);
	}
	
	public function tw_logout()
	{
		//Mandamos a oAuth
		redirect('http://192.241.204.161/api/twtest/clearsession');
	}
	
	public function success()
	{
		//Leemos el Usuario
		$usuario_row = $this->session->userdata('tw_user'); // Objeto
		$tokens = $this->twconnect->tw_get_access_token(); // Array
		$credentials = $this->twconnect->twaccount_verify_credentials(); // Objeto
		
		//Setup Mexico
		date_default_timezone_set('America/Mexico_City');
		
		//Consultamos los Registros del Usuario
		$this->db->where('user_id',(string)$tokens['user_id']);
		$twitters = $this->db->get('twitter');
		
		//Bandera 
		$bandera = 0;
		
		//Procesamos los Registros
		if ($twitters->num_rows() > 0)
		{
			//Leemos el Objeto
			$twitter_row = $twitters->row();
			$bandera = $twitter_row->user_id;
		}
		
		//Verificamos si es Nuevo
		if ($bandera == 0)
		{
			//Generamos el Arreglo para Insertar
			$data = array(
				'user_id' => (string)trim($tokens['user_id']),
				'screen_name' => (string)trim($tokens['screen_name']),
				'name' => (string)trim($credentials->name),
				'avatar' => (string)trim($credentials->profile_image_url),
				'oauth_token' => (string)trim($tokens['oauth_token']),
				'oauth_token_secret' => (string)trim($tokens['oauth_token_secret']),
				'iduser' => (string)trim($usuario_row->iduser),
				'user' => (string)trim($usuario_row->user),
				'status' => (int)1
			);
			$this->db->insert('twitter',$data);
			
			//Leemos el id del registro generado
			$this->db->where('user_id', $tokens['user_id']);
			$this->db->order_by('iduser', 'desc');
			$query = $this->db->get('twitter');
			$query_row = $query->row();
		}
		else
		{
			//Actualizamos los Tokens
			$data = array(
				'screen_name' => (string)trim($tokens['screen_name']),
				'name' => (string)trim($credentials->name),
				'avatar' => (string)trim($credentials->profile_image_url),
				'oauth_token' => (string)trim($tokens['oauth_token']),
				'oauth_token_secret' => (string)trim($tokens['oauth_token_secret']),
				'iduser' => (string)trim($usuario_row->iduser),
				'user' => (string)trim($usuario_row->user),
				'status' => (int)1
			);
			$this->db->where('user_id', $twitter_row->user_id);
			$this->db->update('twitter', $data);
			
			//Leemos el id del registro generado
			$this->db->where('user_id', $twitter_row->user_id);
			$query = $this->db->get('twitter');
			$query_row = $query->row();
		}
		
		//Redirect Error
		redirect('http://192.241.204.161/api/success');
	}
	
	public function test()
	{
		//Revisamos
		$json = $this->twitterapi->getFriends('milioh', '48569547-gVHXm02fcpsJ88JUuaWk1FpcLSlkRc4NlR8evuN4', 'UpsOHNZ1aiXZWuku9wMdZ9Pz87DKaB93kdD6CzRRU');
		
		echo '<pre>';
		print_r($json);
		echo '</pre>';
	}
	
}