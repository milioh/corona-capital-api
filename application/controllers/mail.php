<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mail extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		//Redirigimos al Home
		redirect(base_url());
	}
	
	public function test()
	{
		//Varuables
		$data['to_name'] = 'Emiliano Hernández García';
		$data['to_mail'] = 'emiliano@sclbits.com';
		$data['subject'] = 'Reiniciar Acceso // Corona Capital';
		$data['redirect'] = 'http://apps.sclbits.com/corona/capital/';
		
		//Cuerpo del Correo
		$data['body'] = '<p>Your register is done. You can access to our platform right now.</p>';
		$data['body'] .= '<br /><p><b>Credentials</b></p>';
		$data['body'] .= '<p>Username: <b>'.'milioh'.'</b></p>';
		$data['body'] .= '<p>Password: <b>'.'password'.'</b></p>';
		$data['body'] .= '<br /><br /><p><b>Have Fun!</b></p>';
		
		//Mostramos el Formulario
		$this->load->view('mail_view', $data);
	}
}