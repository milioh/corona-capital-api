<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		//Leemos los parametros
		$json = (isset($_POST['param'])) ? $_POST['param'] : NULL;
		
		//Comprobamos que el parametro no es NULO
		if ($json != NULL)
		{
			//Decodificamos los valores del JSON
			$json_decode = json_decode($json, true);
			
			//Leemos el Mensaje del JSON
			$msg = (isset($json_decode['msg'])) ? $json_decode['msg'] : '';
			
			//Leemos los Campos del JSON
			$fields = (isset($json_decode['fields'])) ? $json_decode['fields'] : '';
			
			//Leemos el Identificador de la App
			$app = (isset($json_decode['app'])) ? $json_decode['app'] : '';
			
			//Leemos el APIKEY de la App
			$apikey = (isset($json_decode['apikey'])) ? $json_decode['apikey'] : '';
				
			//Ejecutamos el JSON que mandaron
			$this->call->executeJSON($msg,$fields,$app,$apikey);
			
		}
		else
		{
			//Respuesta de Error
			$array = array(
				'status' => (int)0,
				'msg' => (string)'JSON not readable.'
			);
				
			//Imprimimos el Arreglo
			$this->call->printJSON($array);
			$output = TRUE;
		}
	}
	
	public function test()
	{
		//Mostramos el Formulario
		$this->load->view('api_view');
	}
}