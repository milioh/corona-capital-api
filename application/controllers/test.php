<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		//Generamos la Consulta
		$query = $this->db->query('SELECT idphoto, image FROM photo WHERE timestamp > "1378011600" AND status = 0 ORDER BY idphoto ASC LIMIT 200');
		
		//Verificamos que haya resultados
		if ($query->num_rows() > 0)
		{
			//Procesamos
			foreach ($query->result() as $row)
			{
				//Activamos la Ultima Foto
				$data = array(
					'status' => 1
				);
				$this->db->where('idphoto', $row->idphoto);
				$this->db->update('photo', $data);
				
				echo 'idphoto: '.$row->idphoto.'<br />imagen: '.$row->image;
				echo '<br /><br />';
			}
		}
		
	}
	
}