<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Call extends CI_Model {

	//Funcion para identar JSON
	public function indent($json) 
	{
	    $result      = '';
	    $pos         = 0;
	    $strLen      = strlen($json);
	    $indentStr   = '  ';
	    $newLine     = "\n";
	    $prevChar    = '';
	    $outOfQuotes = true;
	
	    for ($i=0; $i<=$strLen; $i++) {
	
	        // Grab the next character in the string.
	        $char = substr($json, $i, 1);
	
	        // Are we inside a quoted string?
	        if ($char == '"' && $prevChar != '\\') {
	            $outOfQuotes = !$outOfQuotes;
	        
	        // If this character is the end of an element, 
	        // output a new line and indent the next line.
	        } else if(($char == '}' || $char == ']') && $outOfQuotes) {
	            $result .= $newLine;
	            $pos --;
	            for ($j=0; $j<$pos; $j++) {
	                $result .= $indentStr;
	            }
	        }
	        
	        // Add the character to the result string.
	        $result .= $char;
	
	        // If the last character was the beginning of an element, 
	        // output a new line and indent the next line.
	        if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
	            $result .= $newLine;
	            if ($char == '{' || $char == '[') {
	                $pos ++;
	            }
	            
	            for ($j = 0; $j < $pos; $j++) {
	                $result .= $indentStr;
	            }
	        }
	        
	        $prevChar = $char;
	    }
	
	    return $result;
	}
	
	public function printJSON($array)
	{
		$json = json_encode($array);
		header('Content-Type: application/json',true);
		echo $this->indent($json);
	}
	
	//Manejador de Mensajes Recibidos en el JSON
	public function executeJSON($msg,$fields,$app,$apikey)
	{
		//Leemos la Configuracion
		$this->db->where('app',$app);
		$this->db->where('status',1);
		$application = $this->db->get('app');
		$output = FALSE;
		
		//Ejecutamos el JSON elegido
		if ($application->num_rows() > 0)
		{
			// Leemos el Objeto de la App
			$app_row = $application->row();
			
			// ADMINS
			// doLogin
			if ($msg == 'doLogin')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Consultamos el Login del Usuario
					$this->db->where('username',$fields['username']);
					$usuario = $this->db->get('admin');
					
					//Verificamos el Login
					if ($usuario->num_rows() > 0)
					{
						//Declaramos el Arreglo del Admin
						$admin = $usuario->row();
						
						//Verificamos que el admin este activo
						if ((int)$admin->status == 1)
						{
							//Verificamos la contraseña
							if ((string)trim($admin->password) == sha1($fields['password']))
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Generamos el Arreglo del Admin
								$admin_array = array(
									'idadmin' => (int)$admin->idadmin,
									'username' => (string)trim($admin->username),
									'fullname' => (string)trim($admin->fullname),
									'email' => (string)trim($admin->email),
									'avatar' => (string)trim($admin->avatar),
									'created_at' => (string)trim($admin->created_at),
									'last_login' => (string)trim($admin->last_login),
									'idprofile' => (int)$admin->idprofile,
									'profile' => $this->getProfile($admin->idprofile)
								);
								
								//Actualizamos el Timestamp
								$data = array(
									'last_login' => now()
								);
								$this->db->where('idadmin', (int)$admin->idadmin);
								$this->db->update('admin', $data);
								
								//Unimos el primer arreglo con el resultado
								$array += array(
									'data' => $admin_array
								);
								
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Password invalid. Try Again.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This user is disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user do not exists.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// resetPassword
			if ($msg == 'resetPassword')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Consultamos el Correo
					$this->db->where('email',$fields['email']);
					$usuario = $this->db->get('admin');
					
					//Verificamos el Login
					if ($usuario->num_rows() > 0)
					{
						//Declaramos el Arreglo del Admin
						$admin = $usuario->row();
						
						//Verificamos que el admin este activo
						if ((int)$admin->status == 1)
						{
							//Mandamos el Correo
							
							
							
							//Verificamos la contraseña
							if ((string)trim($admin->password) == sha1($fields['password']))
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Generamos el Arreglo del Admin
								$admin_array = array(
									'idadmin' => (int)$admin->idadmin,
									'username' => (string)trim($admin->username),
									'fullname' => (string)trim($admin->fullname),
									'email' => (string)trim($admin->email),
									'avatar' => (string)trim($admin->avatar),
									'created_at' => (string)trim($admin->created_at),
									'last_login' => (string)trim($admin->last_login),
									'idprofile' => (int)$admin->idprofile,
									'profile' => $this->getProfile($admin->idprofile)
								);
								
								//Actualizamos el Timestamp
								$data = array(
									'last_login' => now()
								);
								$this->db->where('idadmin', (int)$admin->idadmin);
								$this->db->update('admin', $data);
								
								//Unimos el primer arreglo con el resultado
								$array += array(
									'data' => $admin_array
								);
								
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'Password invalid. Try Again.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This user is disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This email do not exists.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// EVENTS
			// getEvents
			if ($msg == 'getEvents')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Consultamos los Eventos Activos
					$this->db->where('status', 1);
					$this->db->order_by('idevent', 'DESC');
					$eventos = $this->db->get('event');
					
					//Verificamos 
					if ($eventos->num_rows() > 0)
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Declaramos el Array de Evento
						$event = array();
						
						//Procesamos los Eventos
						foreach ($eventos->result() as $evento)
						{
							//Generamos el Elemento
							$event[] = array(
								'idevent' => (int)$evento->idevent,
								'name' => (string)trim($evento->name),
								'code' => (string)trim($evento->code),
								'logo' => (string)trim($evento->logo)
							);
						}
								
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $event
						);
								
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no events right now.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getDays
			if ($msg == 'getDays')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Consultamos
					$this->db->where('code', $fields['code']);
					$this->db->where('status', (int)1);
					$evento = $this->db->get('event');
					
					//Verificamos 
					if ($evento->num_rows() > 0)
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Leemos el Objeto
						$evento_row = $evento->row();
						
						//Consultamos los Dias del evento
						$this->db->where('idevent', $evento_row->idevent);
						$dias = $this->db->get('event_day');
						
						//Declaramos el Array de Evento
						$days = array();
						
						//Procesamos los Dias
						foreach ($dias->result() as $dia)
						{
							//Consultamos el Dia
							$this->db->where('idday', $dia->idday);
							$this->db->where('status', (int)1);
							$query = $this->db->get('day');
							
							//Verificamos
							if ($query->num_rows() > 0)
							{
								//Leemos el Objeto
								$row = $query->row();
								
								//Generamos el Elemento
								$days[] = array(
									'idday' => (int)$row->idday,
									'name' => (string)trim($row->name),
									'code' => (string)trim($row->code),
									'date' => (string)trim($row->date)
								);
							}
						}
								
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $days
						);
								
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This day do not exists on this event or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getLineup
			if ($msg == 'getLineup')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					$event = (isset($fields['event'])) ? (string)trim($fields['event']) : '';
					
					//Consultamos
					$this->db->where('code', $event);
					$this->db->where('status', (int)1);
					$evento = $this->db->get('event');
					
					//Verificamos 
					if ($evento->num_rows() > 0)
					{
						//Leer Objeto
						$evento_row = $evento->row();
						
						//Declaramos el Array
						$lineup = array();
					
						//Consultamos los Amigos
						$this->db->where('user', $user);
						$this->db->order_by('idfriendship', 'random');
						$amigos = $this->db->get('friendship');
						
						//Consultamos si tiene Twitter
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$conexion_tw = $this->db->get('twitter');
						
						//Consultamos si tiene Facebook
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$conexion_fb = $this->db->get('facebook');
						
						//Leemos los Días
						$dias = explode(',', $fields['day']);
						
						//Verificamos los Dias
						if (count($dias) > 0)
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Procesamos los Dias
							foreach ($dias as $day)
							{
								//Consultamos el Dia
								$this->db->where('code', trim($day));
								$this->db->where('status', (int)1);
								$dia = $this->db->get('day');
								
								//Verificamos 
								if ($dia->num_rows() > 0)
								{
									//Leer Objeto
									$dia_row = $dia->row();
								
									//Consultamos Relacion Evento - Dia
									$this->db->where('idevent', $evento_row->idevent);
									$this->db->where('idday', $dia_row->idday);
									$relacion = $this->db->get('event_day');
									
									//Verificamos
									if ($relacion->num_rows() > 0)
									{
										//Consultamos el Lineup del Dia
										$this->db->where('idday', $dia_row->idday);
										$alineaciones = $this->db->get('lineup');
										
										//Procesamos el Lineup
										foreach ($alineaciones->result() as $alineacion)
										{
											//Consulta de Banda
											$this->db->where('idband', $alineacion->idband);
											$this->db->where('status', (int)1);
											$banda = $this->db->get('band');
											
											//Verificamos
											if ($banda->num_rows() > 0)
											{
												//Objeto
												$banda_row = $banda->row();
												
												//Procesamos el Arreglo de Banda
												$banda_array = array(
													'idband' => $banda_row->idband,
													'name' => $banda_row->name,
													'slug' => $banda_row->slug,
													'share' => $this->getShare($banda_row->idband)
												);
												
												//Consulta de Escenario
												$this->db->where('idscenario', $alineacion->idscenario);
												$escenario_row = $this->db->get('scenario')->row();
												
												//Procesamos el Arreglo de Banda
												$escenario_array = array(
													'idscenario' => $escenario_row->idscenario,
													'name' => $escenario_row->name,
													'color' => $escenario_row->color
												);
												
												//Declaramos el Arreglo Vacio
												$friends = array();
												
												if ($user != '' && ($conexion_tw->num_rows() > 0 || $conexion_fb->num_rows() > 0))
												{
													//Creamos un Contador
													$contador = 0;
													
													//Procesamos si el amigo agendo la banda
													foreach ($amigos->result() as $amigo)
													{
														//Consultamos si el amigo agendo
														$this->db->where('iduser', $amigo->idfriend);
														$this->db->where('idband', $banda_row->idband);
														$agendo = $this->db->get('scheduled');
														
														//Verificamos
														if ($agendo->num_rows() > 0)
														{
															//Consultamos si tiene conexión con Facebook
															$this->db->where('iduser', $amigo->idfriend);
															$this->db->where('status', 1);
															$facebook = $this->db->get('facebook');
															
															//Consultamos si tiene conexión con Twitter
															$this->db->where('iduser', $amigo->idfriend);
															$this->db->where('status', 1);
															$twitter = $this->db->get('twitter');
															
															//Verificamos
															if ($facebook->num_rows() > 0 || $twitter->num_rows() > 0)
															{
																//Procesamos Facebook
																if ($facebook->num_rows() > 0)
																{
																	//Incrementamos el Contador
																	$contador++;
																	
																	//Leemos el Objeto
																	$facebook_row = $facebook->row();
																	
																	//Generamos el Elemento
																	$friends[] = array(
																		'user' => $amigo->friend,
																		'name' => $facebook_row->fullname,
																		'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
																	);
																}
																else
																{
																	//Incrementamos el Contador
																	$contador++;
																	
																	//Leemos el Objeto
																	$twitter_row = $twitter->row();
																	
																	//Generamos el Elemento
																	$friends[] = array(
																		'user' => $amigo->friend,
																		'name' => '@'.$twitter_row->screen_name,
																		'avatar' => $twitter_row->avatar
																	);
																}
															}
														}
														
														//Rompeamos el Foreach
														/*if ($contador == 5)
														{
															break;
														}*/
													}
												}
												
												//Leemos el número de veces agendado
												$this->db->where('idband', $banda_row->idband);
												$agendados = $this->db->get('scheduled');
												
												//Generamos el Elemento
												$lineup[] = array(
													'idlineup' => (int)$alineacion->idlineup,
													'name' => $banda_row->name,
													'band' => $banda_array,
													'scenario' => $escenario_array,
													'friends' => $friends,
													'start' => (string)trim($alineacion->start),
													'end' => (string)trim($alineacion->end),
													'scheduled' => $agendados->num_rows()
												);
											}
										}
									}
								}
							}
							
							if (!empty($lineup))
							{
								//Verificamos que existe el campo Sort
								if (isset($fields['sort']))
								{
									//Revisamos Ordenacion por Horario
									if ($fields['sort']=='time' || $fields['sort']=='')
									{
										//Ordenamos por Inicio
										$lineup = $this->subval_sort($lineup,'start');
									}
									
									//Revisamos Ordenacion por Banda
									if ($fields['sort']=='band')
									{
										//Ordenamos por Inicio
										$lineup = $this->subval_sort($lineup,'name');
									}
									
									//Revisamos Ordenacion por Agenda
									if ($fields['sort']=='scheduled')
									{
										//Ordenamos por Inicio
										$lineup = $this->subval_sort($lineup,'scheduled', true);
									}
								}
								else
								{
									//Ordenamos por Inicio
									$lineup = $this->subval_sort($lineup,'start');
								}
							}
									
							//Unimos el primer arreglo con el resultado
							$array += array(
								'data' => $lineup
							);
									
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This day do not exists on this event.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This event do not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getLineupMap
			if ($msg == 'getLineupMapOld')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					$event = (isset($fields['event'])) ? (string)trim($fields['event']) : '';
					
					//Consultamos
					$this->db->where('code', $event);
					$this->db->where('status', (int)1);
					$evento = $this->db->get('event');
					
					//Verificamos 
					if ($evento->num_rows() > 0)
					{
						//Leer Objeto
						$evento_row = $evento->row();
						
						//Declaramos el Array
						$lineup = array();
					
						//Consultamos los Amigos
						$this->db->where('user', $user);
						$this->db->order_by('idfriendship', 'random');
						$amigos = $this->db->get('friendship');
						
						//Consultamos si tiene Twitter
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$conexion_tw = $this->db->get('twitter');
						
						//Consultamos si tiene Facebook
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$conexion_fb = $this->db->get('facebook');
						
						//Consultamos los Dias
						$this->db->where('idevent', $evento_row->idevent);
						$dias = $this->db->get('event_day');
						
						//Verificamos los Dias
						if ($dias->num_rows() > 0)
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Declaramos el Arreglo
							$map = array();
							
							//Procesamos los Dias
							foreach ($dias->result() as $row)
							{
								//Consultamos el Dia
								$this->db->where('idday', (int)$row->idday);
								$this->db->where('status', (int)1);
								$dia = $this->db->get('day');
								
								//Verificamos 
								if ($dia->num_rows() > 0)
								{
									//Leer Objeto
									$dia_row = $dia->row();
								
									//Consultamos Relacion Evento - Dia
									$this->db->where('idevent', $evento_row->idevent);
									$this->db->where('idday', $dia_row->idday);
									$relacion = $this->db->get('event_day');
									
									//Verificamos
									if ($relacion->num_rows() > 0)
									{
										//Consultamos el Lineup del Dia
										$this->db->where('idday', $dia_row->idday);
										$alineaciones = $this->db->get('lineup');
										
										//Procesamos el Lineup
										foreach ($alineaciones->result() as $alineacion)
										{
											//Consulta de Banda
											$this->db->where('idband', $alineacion->idband);
											$this->db->where('status', (int)1);
											$banda = $this->db->get('band');
											
											//Verificamos
											if ($banda->num_rows() > 0)
											{
												//Objeto
												$banda_row = $banda->row();
												
												//Procesamos el Arreglo de Banda
												$banda_array = array(
													'idband' => $banda_row->idband,
													'name' => $banda_row->name,
													'slug' => $banda_row->slug,
													'share' => $this->getShare($banda_row->idband)
												);
												
												//Consulta de Escenario
												$this->db->where('idscenario', $alineacion->idscenario);
												$escenario_row = $this->db->get('scenario')->row();
												
												//Procesamos el Arreglo de Banda
												$escenario_array = array(
													'idscenario' => $escenario_row->idscenario,
													'name' => $escenario_row->name,
													'color' => $escenario_row->color
												);
												
												//Declaramos el Arreglo Vacio
												$friends = array();
												
												if ($user != '' && ($conexion_tw->num_rows() > 0 || $conexion_fb->num_rows() > 0))
												{
													//Creamos un Contador
													$contador = 0;
													
													//Procesamos si el amigo agendo la banda
													foreach ($amigos->result() as $amigo)
													{
														//Consultamos si el amigo agendo
														$this->db->where('iduser', $amigo->idfriend);
														$this->db->where('idband', $banda_row->idband);
														$agendo = $this->db->get('scheduled');
														
														//Verificamos
														if ($agendo->num_rows() > 0)
														{
															//Consultamos si tiene conexión con Facebook
															$this->db->where('iduser', $amigo->idfriend);
															$this->db->where('status', 1);
															$facebook = $this->db->get('facebook');
															
															//Consultamos si tiene conexión con Twitter
															$this->db->where('iduser', $amigo->idfriend);
															$this->db->where('status', 1);
															$twitter = $this->db->get('twitter');
															
															//Verificamos
															if ($facebook->num_rows() > 0 || $twitter->num_rows() > 0)
															{
																//Procesamos Facebook
																if ($facebook->num_rows() > 0)
																{
																	//Incrementamos el Contador
																	$contador++;
																	
																	//Leemos el Objeto
																	$facebook_row = $facebook->row();
																	
																	//Generamos el Elemento
																	$friends[] = array(
																		'user' => $amigo->friend,
																		'name' => $facebook_row->fullname,
																		'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
																	);
																}
																else
																{
																	//Incrementamos el Contador
																	$contador++;
																	
																	//Leemos el Objeto
																	$twitter_row = $twitter->row();
																	
																	//Generamos el Elemento
																	$friends[] = array(
																		'user' => $amigo->friend,
																		'name' => '@'.$twitter_row->screen_name,
																		'avatar' => $twitter_row->avatar
																	);
																}
															}
														}
														
														//Rompeamos el Foreach
														/*if ($contador == 5)
														{
															break;
														}*/
													}
												}
												
												//Generamos el Elemento
												$lineup[] = array(
													'idlineup' => (int)$alineacion->idlineup,
													'name' => $banda_row->name,
													'band' => $banda_array,
													'scenario' => $escenario_array,
													'friends' => $friends,
													'start' => (string)trim($alineacion->start),
													'end' => (string)trim($alineacion->end),
													'scheduled' => 0
												);
											}
										}
										
										if (!empty($lineup))
										{
											//Ordenamos por Inicio
											$lineup = $this->subval_sort($lineup,'start');
										}
										
										//Arreglo
										$map[] = array(
											'day' => $dia_row,
											'lineup' => $lineup
										);
									}
								}
							}
							
							//Unimos el primer arreglo con el resultado
							$array += array(
								'data' => $map
							);
									
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This day do not exists on this event.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This event do not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getLineupMap
			if ($msg == 'getLineupMap')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					if ($app=='iPhone')
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'data' => array()
						);
					}
					else
					{
						//Leemos el Usuario
						$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
						$event = (isset($fields['event'])) ? (string)trim($fields['event']) : '';
						
						//Consultamos
						$this->db->where('code', $event);
						$this->db->where('status', (int)1);
						$evento = $this->db->get('event');
						
						//Verificamos 
						if ($evento->num_rows() > 0)
						{
							//Leer Objeto
							$evento_row = $evento->row();
							
							//Declaramos el Array
							$lineup = array();
						
							//Consultamos los Amigos
							$this->db->where('user', $user);
							$this->db->order_by('idfriendship', 'random');
							$amigos = $this->db->get('friendship');
							
							//Consultamos si tiene Twitter
							$this->db->where('user', $user);
							$this->db->where('status', 1);
							$conexion_tw = $this->db->get('twitter');
							
							//Consultamos si tiene Facebook
							$this->db->where('user', $user);
							$this->db->where('status', 1);
							$conexion_fb = $this->db->get('facebook');
							
							//Consultamos los Dias
							$this->db->where('idevent', $evento_row->idevent);
							$dias = $this->db->get('event_day');
							
							//Verificamos los Dias
							if ($dias->num_rows() > 0)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Declaramos el Arreglo
								$map = array();
								
								//Procesamos los Dias
								foreach ($dias->result() as $row)
								{
									//Consultamos el Dia
									$this->db->where('idday', (int)$row->idday);
									$this->db->where('status', (int)1);
									$dia = $this->db->get('day');
									
									//Verificamos 
									if ($dia->num_rows() > 0)
									{
										//Leer Objeto
										$dia_row = $dia->row();
									
										//Consultamos Relacion Evento - Dia
										$this->db->where('idevent', $evento_row->idevent);
										$this->db->where('idday', $dia_row->idday);
										$relacion = $this->db->get('event_day');
										
										//Verificamos
										if ($relacion->num_rows() > 0)
										{
											//Consultamos el Lineup del Dia
											$this->db->where('idday', $dia_row->idday);
											$alineaciones = $this->db->get('lineup');
											
											//Procesamos el Lineup
											foreach ($alineaciones->result() as $alineacion)
											{
												//Consulta de Banda
												$this->db->where('idband', $alineacion->idband);
												$this->db->where('status', (int)1);
												$banda = $this->db->get('band');
												
												//Verificamos
												if ($banda->num_rows() > 0)
												{
													//Objeto
													$banda_row = $banda->row();
													
													//Procesamos el Arreglo de Banda
													$banda_array = array(
														'idband' => $banda_row->idband,
														'name' => $banda_row->name,
														'slug' => $banda_row->slug,
														'share' => $this->getShare($banda_row->idband)
													);
													
													//Consulta de Escenario
													$this->db->where('idscenario', $alineacion->idscenario);
													$escenario_row = $this->db->get('scenario')->row();
													
													//Procesamos el Arreglo de Banda
													$escenario_array = array(
														'idscenario' => $escenario_row->idscenario,
														'name' => $escenario_row->name,
														'color' => $escenario_row->color
													);
													
													//Declaramos el Arreglo Vacio
													$friends = array();
													
													if ($user != '' && ($conexion_tw->num_rows() > 0 || $conexion_fb->num_rows() > 0))
													{
														//Creamos un Contador
														$contador = 0;
														
														//Procesamos si el amigo agendo la banda
														foreach ($amigos->result() as $amigo)
														{
															//Consultamos si el amigo agendo
															$this->db->where('iduser', $amigo->idfriend);
															$this->db->where('idband', $banda_row->idband);
															$agendo = $this->db->get('scheduled');
															
															//Verificamos
															if ($agendo->num_rows() > 0)
															{
																//Consultamos si tiene conexión con Facebook
																$this->db->where('iduser', $amigo->idfriend);
																$this->db->where('status', 1);
																$facebook = $this->db->get('facebook');
																
																//Consultamos si tiene conexión con Twitter
																$this->db->where('iduser', $amigo->idfriend);
																$this->db->where('status', 1);
																$twitter = $this->db->get('twitter');
																
																//Verificamos
																if ($facebook->num_rows() > 0 || $twitter->num_rows() > 0)
																{
																	//Procesamos Facebook
																	if ($facebook->num_rows() > 0)
																	{
																		//Incrementamos el Contador
																		$contador++;
																		
																		//Leemos el Objeto
																		$facebook_row = $facebook->row();
																		
																		//Procesamos la Imagen
																		$avatar_json = $this->curl->simple_get('https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture?redirect=false&access_token='.$facebook_row->access_token_long);
																		$avatar_array = json_decode($avatar_json, true);
																		$avatar = $avatar_array['data']['url'];
																		
																		//Generamos el Elemento
																		$friends[] = array(
																			'user' => $amigo->friend,
																			'name' => $facebook_row->fullname,
																			'avatar' => $avatar
																		);
																	}
																	else
																	{
																		//Incrementamos el Contador
																		$contador++;
																		
																		//Leemos el Objeto
																		$twitter_row = $twitter->row();
																		
																		//Generamos el Elemento
																		$friends[] = array(
																			'user' => $amigo->friend,
																			'name' => '@'.$twitter_row->screen_name,
																			'avatar' => $twitter_row->avatar
																		);
																	}
																}
															}														
														}
													}
													
													//Generamos el Elemento
													$lineup[] = array(
														'idlineup' => (int)$alineacion->idlineup,
														'name' => $banda_row->name,
														'band' => $banda_array,
														'scenario' => $escenario_array,
														'friends' => $friends,
														'start' => (string)trim($alineacion->start),
														'end' => (string)trim($alineacion->end),
														'scheduled' => 0
													);
												}
											}
											
											if (!empty($lineup))
											{
												//Ordenamos por Inicio
												$lineup = $this->subval_sort($lineup,'start');
											}
											
											//Arreglo
											$map[] = array(
												'day' => $dia_row,
												'lineup' => $lineup
											);
										}
									}
								}
								
								//Unimos el primer arreglo con el resultado
								$array += array(
									'data' => $map
								);
										
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This day do not exists on this event.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This event do not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
				}
			}
			
			// getFriendSchedule
			if ($msg == 'getFriendSchedule')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					$friend = (isset($fields['friend'])) ? (string)trim($fields['friend']) : '';
					$event = (isset($fields['event'])) ? (string)trim($fields['event']) : '';
					
					//Consultamos el Usuario
					$this->db->where('user', $friend);
					$this->db->where('status', 1);
					$amigo = $this->db->get('user');
					
					//Verificamos
					if ($amigo->num_rows() > 0)
					{
						//Leemos el Objeto
						$amigo_row = $amigo->row();
						
						//Declaramos el Arreglo
						$scheduled = array();
					
						//Consultamos
						$this->db->where('code', $event);
						$this->db->where('status', (int)1);
						$evento = $this->db->get('event');
						
						//Verificamos 
						if ($evento->num_rows() > 0)
						{
							//Leer Objeto
							$evento_row = $evento->row();
							
							//Leemos los Días
							$dias = explode(',', $fields['day']);
							
							//Verificamos los Dias
							if (count($dias) > 0)
							{
								//Procesamos los Dias
								foreach ($dias as $day)
								{
									//Consultamos el Dia
									$this->db->where('code', trim($day));
									$this->db->where('status', (int)1);
									$dia = $this->db->get('day');
									
									//Verificamos 
									if ($dia->num_rows() > 0)
									{
										//Leer Objeto
										$dia_row = $dia->row();
									
										//Consultamos Relacion Evento - Dia
										$this->db->where('idevent', $evento_row->idevent);
										$this->db->where('idday', $dia_row->idday);
										$relacion = $this->db->get('event_day');
										
										//Verificamos
										if ($relacion->num_rows() > 0)
										{
											//Consultamos las Bandas Agendadas por el Amigo
											$this->db->where('iduser', $amigo_row->iduser);
											$agendados = $this->db->get('scheduled');
											
											//Generamos el Arreglo
											$array = array(
												'status' => (int)1,
												'msg' => 'success'
											);
											
											//Procesamos las Bandas
											foreach ($agendados->result() as $agendado)
											{
												//Consultamos si es una banda del dia seleccionado
												$this->db->where('idband', $agendado->idband);
												$this->db->where('idday', $dia_row->idday);
												$query = $this->db->get('lineup');
												
												//Verificamos
												if ($query->num_rows() > 0)
												{
													//Leemos el Objeto
													$row = $query->row();
													
													//Generamos el Elemento
													$scheduled[] = array(
														'idlineup' => $row->idlineup
													);
												}
											}
											
											//Consultamos el Usuario
											$this->db->where('user', $user);
											$this->db->where('status', 1);
											$usuario = $this->db->get('user')->row();
											
											//Declaramos 
											$array_user = array();
											
											//Consultamos Facebook
											$this->db->where('user', $usuario->user);
											$this->db->where('status', 1);
											$facebook = $this->db->get('facebook');
											
											//Verificamos
											if ($facebook->num_rows() > 0)
											{
												//Leemos el Objeto
												$facebook_row = $facebook->row();
												
												//Generamos el Elemento
												$array_user = array(
													'user' => $usuario->user,
													'name' => $facebook_row->fullname,
													'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
												);
											}
											else
											{
												//Consultamos Twitter
												$this->db->where('user', $usuario->user);
												$this->db->where('status', 1);
												$twitter = $this->db->get('twitter');
												
												//Verificamos
												if ($twitter->num_rows() > 0)
												{
													//Leemos el Objeto
													$twitter_row = $twitter->row();
													
													//Generamos el Elemento
													$array_user = array(
														'user' => $usuario->user,
														'name' => '@'.$twitter_row->screen_name,
														'avatar' => $twitter_row->avatar
													);
												}
											}
											
											//Declaramos 
											$array_friend = array();
											
											//Consultamos Facebook
											$this->db->where('user', $amigo_row->user);
											$this->db->where('status', 1);
											$facebook = $this->db->get('facebook');
											
											//Verificamos
											if ($facebook->num_rows() > 0)
											{
												//Leemos el Objeto
												$facebook_row = $facebook->row();
												
												//Generamos el Elemento
												$array_friend = array(
													'user' => $amigo_row->user,
													'name' => $facebook_row->fullname,
													'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
												);
											}
											else
											{
												//Consultamos Twitter
												$this->db->where('user', $amigo_row->user);
												$this->db->where('status', 1);
												$twitter = $this->db->get('twitter');
												
												//Verificamos
												if ($twitter->num_rows() > 0)
												{
													//Leemos el Objeto
													$twitter_row = $twitter->row();
													
													//Generamos el Elemento
													$array_friend = array(
														'user' => $amigo_row->user,
														'name' => '@'.$twitter_row->screen_name,
														'avatar' => $twitter_row->avatar
													);
												}
											}
											
											//Consultamos si son amigos
											$this->db->where('user', $array_user['user']);
											$this->db->where('friend', $array_friend['user']);
											$es_amigo = $this->db->get('friendship');
											
											//Verificamos
											if ($es_amigo->num_rows() > 0)
											{
												//Es Amigo
												$favorited = 1;
											}
											else
											{
												//No Es Amigo
												$favorited = 0;
											}
											
											//Generamos la Agenda
											$agenda = array(
												'user' => $array_user,
												'friend' => $array_friend,
												'scheduled' => $scheduled,
												'favorited' => $favorited
											);
											
											//Unimos el primer arreglo con el resultado
											$array += array(
												'data' => $agenda
											);
													
											//Imprimimos el Arreglo
											$this->printJSON($array);
											$output = TRUE;
										}
										else
										{
											//Respuesta de Error
											$array = array(
												'status' => (int)0,
												'msg' => (string)'This day is not related to this event.'
											);
												
											//Imprimimos el Arreglo
											$this->printJSON($array);
											$output = TRUE;
										}
									}
									else
									{
										//Respuesta de Error
										$array = array(
											'status' => (int)0,
											'msg' => (string)'This day does not exists or its disabled.'
										);
											
										//Imprimimos el Arreglo
										$this->printJSON($array);
										$output = TRUE;
									}
								}
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This call needs set a day to work.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This event does not exists or is disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user does not exists or is disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// USERS
			// createUser
			if ($msg == 'createUser')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Obtenemos un ID
					$id = $this->getNewID();
					
					//Consultamos en la Base
					$this->db->where('user', $id);
					$usuario = $this->db->get('user');
					
					while ($usuario->num_rows() > 0)
					{
						//Obtenemos un ID
						$id = $this->getNewID();
						
						//Consultamos en la Base
						$this->db->where('user', $id);
						$usuario = $this->db->get('user');
					}
					
					$now = now();
					
					//Generamos el Nuevo Usuario
					$data = array(
						'user' => (string)trim($id),
						'created_at' => $now,
						'status' => (int)1
					);
					$this->db->insert('user', $data);
					
					//Leemos el Ultimo Registro
					$this->db->order_by('iduser', 'DESC');
					$row = $this->db->get('user')->row();
					
					//Generamos el Log del Nuevo Usuario
					$data = array(
						'iduser' => $row->iduser,
						'action' => 'Creación de Nuevo Usuario',
						'rel' => (string)trim($id),
						'timestamp' => $now
					);
					$this->db->insert('user_log', $data);
					
					//Generamos el Arreglo
					$array = array(
						'status' => (int)1,
						'msg' => 'success'
					);
					
					//Generamos el Arreglo
					$user = array(
						'user' => $row->user,
						'created_at' => $row->created_at
					);
					
					//Unimos el primer arreglo con el resultado
					$array += array(
						'data' => $user
					);
							
					//Imprimimos el Arreglo
					$this->printJSON($array);
					$output = TRUE;
				}
			}
			
			// statusUser
			if ($msg == 'statusUser')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Verificamos
					if ($user != '')
					{
						//Consultamos el Usuario
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$usuario = $this->db->get('user');
						
						//Verificamos
						if ($usuario->num_rows() > 0)
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Leemos el Objeto
							$usuario_row = $usuario->row();
							$facebook_user = false;
							$twitter_user = false;
							
							//Consultamos Facebook
							$this->db->where('iduser', $usuario_row->iduser);
							$this->db->where('status', 1);
							$facebook = $this->db->get('facebook');
							
							//Verificamos
							if ($facebook->num_rows() > 0) { $facebook_user = true; }
							
							//Consultamos Twitter
							$this->db->where('iduser', $usuario_row->iduser);
							$this->db->where('status', 1);
							$twitter = $this->db->get('twitter');
							
							//Verificamos
							if ($twitter->num_rows() > 0) { $twitter_user = true; }
							
							//Redes Sociales
							$networks = array(
								'facebook' => ($facebook_user) ? 1 : 0,
								'twitter' => ($twitter_user) ? 1 : 0
							);
							
							//Unimos el primer arreglo con el resultado
							$array += array(
								'data' => $networks
							);
									
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This user do not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user do not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// BANDS
			// scheduleBand
			if ($msg == 'scheduleBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID del Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Consultamos que exista el Usuario
					$this->db->where('user', $user);
					$this->db->where('status', 1);
					$usuario = $this->db->get('user');
					
					//Verificamos
					if ($usuario->num_rows() > 0)
					{
						//Leemos el Objeto
						$usuario_row = $usuario->row();
						
						//Leemos la Banda
						$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
						
						//Consultamos la Banda
						$this->db->where('idband', $idband);
						$this->db->where('status', 1);
						$banda = $this->db->get('band');
						
						//Verificamos
						if ($banda->num_rows() > 0)
						{
							//Leemos el Objeto
							$banda_row = $banda->row();
							
							//Consultamos que no exista la relacion
							$this->db->where('idband', $banda_row->idband);
							$this->db->where('iduser', $usuario_row->iduser);
							$relacion = $this->db->get('scheduled');
							
							//Verificamos
							if ($relacion->num_rows() == 0)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Generamos la Relación
								$data = array(
									'idband' => $banda_row->idband,
									'iduser' => $usuario_row->iduser
								);
								$this->db->insert('scheduled', $data);
								
								//Leemos la Relacion
								$this->db->where('idband', $banda_row->idband);
								$this->db->where('iduser', $usuario_row->iduser);
								$relacion = $this->db->get('scheduled')->row();
								
								//Generamos la Respuesta
								$scheduled = array(
									'idscheduled' => $relacion->idscheduled,
									'idband' => $relacion->idband,
									'iduser' => $relacion->iduser
								);
								
								//Unimos el primer arreglo con el resultado
								$array += array(
									'data' => $scheduled
								);
								
								//Generamos el Log
								$data = array(
									'iduser' => $usuario_row->iduser,
									'action' => 'Agendar Banda',
									'rel' => $banda_row->idband,
									'timestamp' => now()
								);
								$this->db->insert('user_log', $data);
										
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This schedule already exists.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This band does not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user do not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// BANDS
			// scheduleBand
			if ($msg == 'scheduleBands')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID del Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					$bands = (isset($fields['bands'])) ? (string)trim($fields['bands']) : '';
					
					//Consultamos que exista el Usuario
					$this->db->where('user', $user);
					$this->db->where('status', 1);
					$usuario = $this->db->get('user');
					
					//Verificamos
					if ($usuario->num_rows() > 0)
					{
						//Leemos el Objeto
						$usuario_row = $usuario->row();
						
						//Leemos las Banda
						$bandas = explode(',', $bands);
						$scheduled = array();
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Procesamos las Bandas
						foreach ($bandas as $row)
						{
							//Consultamos la Banda
							$this->db->where('idband', (string)trim($row));
							$this->db->where('status', 1);
							$banda = $this->db->get('band');
							
							//Verificamos
							if ($banda->num_rows() > 0)
							{
								//Leemos el Objeto
								$banda_row = $banda->row();
								
								//Consultamos que no exista la relacion
								$this->db->where('idband', $banda_row->idband);
								$this->db->where('iduser', $usuario_row->iduser);
								$relacion = $this->db->get('scheduled');
								
								//Verificamos
								if ($relacion->num_rows() == 0)
								{
									//Generamos la Relación
									$data = array(
										'idband' => $banda_row->idband,
										'iduser' => $usuario_row->iduser
									);
									$this->db->insert('scheduled', $data);
									
									//Leemos la Relacion
									$this->db->where('idband', $banda_row->idband);
									$this->db->where('iduser', $usuario_row->iduser);
									$relacion = $this->db->get('scheduled')->row();
									
									//Generamos la Respuesta
									$scheduled[] = array(
										'idscheduled' => $relacion->idscheduled,
										'idband' => $relacion->idband,
										'iduser' => $relacion->iduser
									);
									
									//Generamos el Log
									$data = array(
										'iduser' => $usuario_row->iduser,
										'action' => 'Agendar Banda',
										'rel' => $banda_row->idband,
										'timestamp' => now()
									);
									$this->db->insert('user_log', $data);
								}
							}
						}
						
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $scheduled
						);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user do not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// removeBand
			if ($msg == 'removeBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID del Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Consultamos que exista el Usuario
					$this->db->where('user', $user);
					$this->db->where('status', 1);
					$usuario = $this->db->get('user');
					
					//Verificamos
					if ($usuario->num_rows() > 0)
					{
						//Leemos el Objeto
						$usuario_row = $usuario->row();
						
						//Leemos la Banda
						$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
						
						//Consultamos la Banda
						$this->db->where('idband', $idband);
						$this->db->where('status', 1);
						$banda = $this->db->get('band');
						
						//Verificamos
						if ($banda->num_rows() > 0)
						{
							//Leemos el Objeto
							$banda_row = $banda->row();
							
							//Consultamos que no exista la relacion
							$this->db->where('idband', $banda_row->idband);
							$this->db->where('iduser', $usuario_row->iduser);
							$relacion = $this->db->get('scheduled');
							
							//Verificamos
							if ($relacion->num_rows() > 0)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Leemos el Objeto
								$relacion_row = $relacion->row();
								
								//Eliminamos la Relación
								$this->db->where('idscheduled', $relacion_row->idscheduled);
								$this->db->delete('scheduled');
								
								//Generamos el Log
								$data = array(
									'iduser' => $usuario_row->iduser,
									'action' => 'Eliminar Banda',
									'rel' => $banda_row->idband,
									'timestamp' => now()
								);
								$this->db->insert('user_log', $data);
										
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This schedule does not exists.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This band does not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user do not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getInfoBand
			if ($msg == 'getInfoBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el id band
					$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
					$slug = (isset($fields['slug'])) ? (string)trim($fields['slug']) : '';
					
					//Consultamos la Banda
					if ($slug == '')
					{
						$this->db->where('idband', $idband);
					}
					else
					{
						$this->db->where('slug', $slug);
					}
					$this->db->where('status', (int)1);
					$banda = $this->db->get('band');
					
					//Verificamos
					if ($banda->num_rows() > 0)
					{
						//Leemos el Objeto
						$banda_row = $banda->row();
						
						//Consultamos la Info de la Banda
						$this->db->where('idband', $banda_row->idband);
						$informacion = $this->db->get('info_band');
						
						//Verificamos
						if ($informacion->num_rows() > 0)
						{
							//Leemos el Objeto
							$informacion_row = $informacion->row();
							
							//Consultamos su posicion en el LineUp
							$this->db->where('idband', $banda_row->idband);
							$this->db->where('status', 1);
							$alineacion = $this->db->get('lineup');
							
							//Verificamos
							if ($alineacion->num_rows() > 0)
							{
								//Leemos el Objeto
								$alineacion_row = $alineacion->row();
								
								//Consultamos el dia
								$this->db->where('idday', $alineacion_row->idday);
								$this->db->where('status', 1);
								$dia = $this->db->get('day');
								
								//Verificamos
								if ($dia->num_rows() > 0)
								{
									//Leemos el Objeto
									$dia_row = $dia->row();
									
									//Leemos el Escenario
									$this->db->where('idscenario', $alineacion_row->idscenario);
									$escenario_row = $this->db->get('scenario')->row();
									
									//Generamos el Arreglo
									$array = array(
										'status' => (int)1,
										'msg' => 'success'
									);
									
									//Verificamos 
									if ($app!='Android')
									{
										//Guardamos el Video para iOS
										$video = '<html><body style="background-color:#D2e0df; text-align:center; margin:0; padding:0;"><iframe style="margin:0 auto;" width="320" height="240" src="'.$informacion_row->video.'?showinfo=0" frameborder="0" allowfullscreen></iframe></body></html>';
									}
									else
									{
										//Android Video
										$android_video = explode('/', $informacion_row->video);
										
										//Guardamos el Video para Android
										$video = $android_video[count($android_video)-1];
									}
									
									//Generamos el Arreglo
									$band = array(
										'idband' => $banda_row->idband,
										'name' => $banda_row->name,
										'slug' => $banda_row->slug,
										'share' => $this->getShare($banda_row->idband),
										'info' => array(
											'text' => $informacion_row->info,
											'featured_image' => $informacion_row->featured_image,
											'site_image' => $informacion_row->site_image,
											'url' => $informacion_row->url,
											'video' => $video,
											'twitter' => $informacion_row->twitter,
											'facebook' => $informacion_row->facebook
										),
										'scenario' => array(
											'idscenario' => $escenario_row->idscenario,
											'name' => $escenario_row->name,
											'color' => $escenario_row->color
										),
										'lineup' => array(
											'idday' => $dia_row->idday,
											'name' => $dia_row->name,
											'code' => $dia_row->code,
											'date' => $dia_row->date,
											'start' => $alineacion_row->start,
											'end' => $alineacion_row->end
										)
									);
									
									//Unimos el primer arreglo con el resultado
									$array += array(
										'data' => $band
									);
									
									//Leemos el ID del Usuario
									$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
									
									//Consultamos el Usuario
									$this->db->where('user', $user);
									$this->db->where('status', 1);
									$usuario = $this->db->get('user');
									
									//Verificamos
									if ($usuario->num_rows() > 0)
									{
										//Leemos el Objeto
										$usuario_row = $usuario->row();
										
										//Generamos el Log
										$data = array(
											'iduser' => $usuario_row->iduser,
											'action' => 'Leer Info de Banda',
											'rel' => $banda_row->idband,
											'timestamp' => now()
										);
										$this->db->insert('user_log', $data);
									}
											
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
								else
								{
									//Respuesta de Error
									$array = array(
										'status' => (int)0,
										'msg' => (string)'This day does not exists on this event or its disabled.'
									);
										
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This band does not have a event scheduled.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This band does not have information yet.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}  
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This band does not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getInfoBand
			if ($msg == 'getFriendsBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos las Variables
					$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Verificamos
					if ($idband != 0 && $user != '')
					{
						//Consultamos la Banda
						$this->db->where('idband', $idband);
						$this->db->where('status', (int)1);
						$banda = $this->db->get('band');
						
						//Verificamos
						if ($banda->num_rows() > 0)
						{
							//Leemos el Objeto
							$banda_row = $banda->row();
							
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Consultamos los Amigos
							$this->db->where('user', $user);
							$amigos = $this->db->get('friendship');
							
							//Declaramos el Arreglo de Amigos
							$friends = array();
							
							//Procesamos los Amigos con la Banda
							foreach ($amigos->result() as $row)
							{
								//Consultamos si el amigo agendo la banda
								$this->db->where('iduser', $row->idfriend);
								$this->db->where('idband', $banda_row->idband);
								$agendo = $this->db->get('scheduled');
								
								//Verificamos
								if ($agendo->num_rows() > 0)
								{
									//Consultamos si tiene conexión con Facebook
									$this->db->where('iduser', $row->idfriend);
									$this->db->where('status', 1);
									$facebook = $this->db->get('facebook');
									
									//Consultamos si tiene conexión con Twitter
									$this->db->where('iduser', $row->idfriend);
									$this->db->where('status', 1);
									$twitter = $this->db->get('twitter');
									
									//Verificamos
									if ($facebook->num_rows() > 0 || $twitter->num_rows() > 0)
									{		
										//Caso FACEBOOK
										if ($facebook->num_rows() > 0)
										{			
											//Leemos el Objeto
											$facebook_row = $facebook->row();
																						
											//Generamos el arreglo
											$friends[] = array(
												'network' => 'facebook',
												'network_name' => $facebook_row->fullname,
												'network_id' => $facebook_row->facebook_id,
												'network_avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture',
												'iduser' => $row->idfriend,
												'user' => $row->friend,
												'favorited' => (int)1
											);
										}
										else
										{
											//Leemos el Objeto
											$twitter_row = $twitter->row();
											
											//Generamos el arreglo
											$friends[] = array(
												'network' => 'twitter',
												'network_name' => '@'.$twitter_row->screen_name,
												'network_id' => $twitter_row->user_id,
												'network_avatar' => $twitter_row->avatar,
												'iduser' => $row->idfriend,
												'user' => $row->friend,
												'favorited' => (int)1
											);
										}
									}
								}
							}
							
							//Unimos el primer arreglo con el resultado
							$array += array(
								'data' => $friends
							);
							
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This band does not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This call needs a idband and an user.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getWallBand
			if ($msg == 'getWallBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el id band
					$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
					$user_string = (isset($fields['user'])) ? (int)$fields['user'] : '';
					$offset = (isset($fields['offset'])) ? (int)$fields['offset'] : 0;
					
					//Verificamos que la Banda exista
					$this->db->where('idband', $idband);
					$this->db->where('status', 1);
					$banda = $this->db->get('band');
					
					//Verificamos
					if ($banda->num_rows() > 0)
					{
						//Consultamos los Mensajes
						$this->db->where('idband', $idband);
						$this->db->where('status', (int)1);
						$this->db->order_by('timestamp', 'DESC');
						$mensajes = $this->db->get('wall_band', 50, $offset);
						
						//Declaramos el Contador
						$contador = 0;
						
						//Verificamos
						if ($mensajes->num_rows() > 0)
						{
							//Declaramos el Arreglo de Mensajes
							$messages = array();
							
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Procesamos los Mensajes
							foreach ($mensajes->result() as $mensaje)
							{
								//Consultamos el Usuario
								$this->db->where('iduser', $mensaje->iduser);
								$this->db->where('status', 1);
								$usuario = $this->db->get('user');
								
								//Verificamos 
								if ($usuario->num_rows() > 0)
								{
									//Declaramos el Arreglo de Usuario
									$user = array();
									
									//Usuario Facebook
									$this->db->where('iduser', $mensaje->iduser);
									$this->db->where('status', 1);
									$facebook = $this->db->get('facebook');
									
									//Verificamos
									if ($facebook->num_rows() > 0)
									{
										//Leemos el Objeto
										$facebook_row = $facebook->row();
										
										//Generamos el Usuario
										$user = array(
											'user' => $facebook_row->user,
											'name' => $facebook_row->first_name,
											'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
										);
									}
									else
									{
										//Usuario Twitter
										$this->db->where('iduser', $mensaje->iduser);
										$this->db->where('status', 1);
										$twitter = $this->db->get('twitter');
										
										//Verificamos
										if ($twitter->num_rows() > 0)
										{
											//Leemos el Objeto
											$twitter_row = $twitter->row();
											
											//Generamos el Usuario
											$user = array(
												'user' => $twitter_row->user,
												'name' => $twitter_row->name,
												'avatar' => $twitter_row->avatar
											);
										}
									}
									
									//Verificamos que tenga usuario conectado
									if (count($user) > 0)
									{
										//Aumentamos el Contador
										$contador++;
										
										//Revisamos si es menor a 21
										if ($contador < 21)
										{
											//Revisamos si esta Reportado por el usuario
											$this->db->where('idwall_band', $mensaje->idwall_band);
											$this->db->where('idband', $idband);
											$this->db->where('user', $user_string);
											$reportado = $this->db->get('report_wall_band');
																					
											//Generamos el Elemento
											$messages[] = array(
												'idwall_band' => (int)$mensaje->idwall_band,
												'text' => (string)trim($mensaje->text),
												'timestamp' => (string)trim($mensaje->timestamp),
												'timestamp_string' => $this->getTimestampString($mensaje->timestamp),
												'date' => (string)trim($mensaje->date),
												'user' => $user,
												'reported' => ($reportado->num_rows() > 0) ? 1 : 0
											);
										}
									}
								}
								
								//Verificamos Contadores
								if ($contador == 21)
								{
									break;
								}
							}
							
							//Ponemos la Paginación
							$pages = array(
								'messages' => $messages,
								'next' => ($contador==21) ? 1 : 0
							);
							
							//Unimos el primer arreglo con el resultado
							$array += array(
								'data' => $pages
							);
							
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success',
								'data' => array()
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This band does not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// setWallMsgBand
			if ($msg == 'setWallMsgBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos las Variables
					$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Verificamos
					if ($idband != 0 && $user != '')
					{
						//Consultamos la Banda
						$this->db->where('idband', $idband);
						$this->db->where('status', 1);
						$banda = $this->db->get('band');
						
						//Verificamos
						if ($banda->num_rows() > 0)
						{
							//Consultamos el Usuario
							$this->db->where('user', $user);
							$this->db->where('status', 1);
							$usuario = $this->db->get('user');
							
							//Verificamos
							if ($usuario->num_rows() > 0)
							{
								//Leemos el Objeto
								$usuario_row = $usuario->row();
								
								//Declaramos el Arreglo de Usuario
								$user = false;
								
								//Usuario Facebook
								$this->db->where('iduser', $usuario_row->iduser);
								$this->db->where('status', 1);
								$facebook = $this->db->get('facebook');
								
								//Verificamos
								if ($facebook->num_rows() > 0)
								{
									//Activamos
									$user = true;
								}
								else
								{
									//Usuario Twitter
									$this->db->where('iduser', $usuario_row->iduser);
									$this->db->where('status', 1);
									$twitter = $this->db->get('twitter');
									
									//Verificamos
									if ($twitter->num_rows() > 0)
									{
										//Activamos
										$user = true;
									}
								}
								
								//Verificamos que tenga usuario conectado
								if ($user)
								{
									//Generamos el Arreglo
									$array = array(
										'status' => (int)1,
										'msg' => 'success'
									);
							
									//Generamos el Elemento
									$data = array(
										'text' => (string)trim($fields['text']),
										'timestamp' => now(),
										'date' => date('Y-m-d h:i:s'),
										'idband' => (int)$idband,
										'iduser' => $usuario_row->iduser,
										'status' => (int)1
									);
									$this->db->insert('wall_band', $data);
									
									//Consultamos los Mensajes
									$this->db->where('idband', $idband);
									$this->db->where('status', (int)1);
									$this->db->order_by('timestamp', 'DESC');
									$mensajes = $this->db->get('wall_band', 50, 0);
									
									//Declaramos el Contador
									$contador = 0;
									
									//Verificamos
									if ($mensajes->num_rows() > 0)
									{
										//Declaramos el Arreglo de Mensajes
										$messages = array();
										
										//Procesamos los Mensajes
										foreach ($mensajes->result() as $mensaje)
										{
											//Consultamos el Usuario
											$this->db->where('iduser', $mensaje->iduser);
											$this->db->where('status', 1);
											$usuario = $this->db->get('user');
											
											//Verificamos 
											if ($usuario->num_rows() > 0)
											{
												//Declaramos el Arreglo de Usuario
												$user = array();
												
												//Usuario Facebook
												$this->db->where('iduser', $mensaje->iduser);
												$this->db->where('status', 1);
												$facebook = $this->db->get('facebook');
												
												//Verificamos
												if ($facebook->num_rows() > 0)
												{
													//Leemos el Objeto
													$facebook_row = $facebook->row();
													
													//Generamos el Usuario
													$user = array(
														'user' => $facebook_row->user,
														'name' => $facebook_row->first_name,
														'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
													);
												}
												else
												{
													//Usuario Twitter
													$this->db->where('iduser', $mensaje->iduser);
													$this->db->where('status', 1);
													$twitter = $this->db->get('twitter');
													
													//Verificamos
													if ($twitter->num_rows() > 0)
													{
														//Leemos el Objeto
														$twitter_row = $twitter->row();
														
														//Generamos el Usuario
														$user = array(
															'user' => $twitter_row->user,
															'name' => $twitter_row->name,
															'avatar' => $twitter_row->avatar
														);
													}
												}
												
												//Verificamos que tenga usuario conectado
												if (count($user) > 0)
												{
													//Aumentamos el Contador
													$contador++;
													
													//Revisamos si es menor a 21
													if ($contador < 21)
													{										
														//Generamos el Elemento
														$messages[] = array(
															'idwall_band' => (int)$mensaje->idwall_band,
															'text' => (string)trim($mensaje->text),
															'timestamp' => (string)trim($mensaje->timestamp),
															'timestamp_string' => $this->getTimestampString($mensaje->timestamp),
															'date' => (string)trim($mensaje->date),
															'user' => $user
														);
													}
												}
											}
											
											//Verificamos Contadores
											if ($contador == 21)
											{
												break;
											}
										}
									}
										
									//Ponemos la Paginación
									$pages = array(
										'messages' => $messages,
										'next' => ($contador==21) ? 1 : 0
									);
									
									//Unimos el primer arreglo con el resultado
									$array += array(
										'data' => $pages
									);
									
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
								else
								{
									//Respuesta de Error
									$array = array(
										'status' => (int)0,
										'msg' => (string)'This user does not have connected social networks or its disabled.'
									);
										
									//Imprimimos el Arreglo
									$this->printJSON($array);
									$output = TRUE;
								}
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This user does not exists or is disabled.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This band does not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This call needs a band and an user to work.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// reportWallMsgBand
			if ($msg == 'reportWallMsgBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID del Mensaje
					$idwall_band = (isset($fields['idwall_band'])) ? (int)$fields['idwall_band'] : 0;
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Consultamos el Mensaje
					$this->db->where('idwall_band', $idwall_band);
					$this->db->where('status', 1);
					$mensaje = $this->db->get('wall_band');
					
					//Verificamos
					if ($mensaje->num_rows() > 0)
					{
						//Leemos el Objeto
						$mensaje_row = $mensaje->row();
						
						//Consultamos el Usuario
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$usuario = $this->db->get('user');
						
						//Verificamos
						if ($usuario->num_rows() > 0)
						{
							//Leemos el Objeto
							$usuario_row = $usuario->row();
							
							//Consultamos que no exista el Reporte
							$this->db->where('idwall_band', $mensaje_row->idwall_band);
							$this->db->where('iduser', $usuario_row->iduser);
							$relacion = $this->db->get('report_wall_band');
							
							//Verificamos
							if ($relacion->num_rows() == 0)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Generamos el Arreglo
								$data = array(
									'idband' => $mensaje_row->idband,
									'idwall_band' => $idwall_band,
									'iduser' => $usuario_row->iduser,
									'user' => $usuario_row->user
								);
								$this->db->insert('report_wall_band', $data);
								
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This message is already report by this user.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This call needs an user to work.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This call needs an idwall_band to work.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// FRIENDS
			// getFriends
			if ($msg == 'getFriends')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el Usuario
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Consultamos el Usuario
					$this->db->where('user', $user);
					$this->db->where('status', 1);
					$usuario = $this->db->get('user');
					
					//Verificamos
					if ($usuario->num_rows() > 0)
					{
						//Leemos el Objeto
						$usuario_row = $usuario->row();
						
						//Consultamos si tiene conexión con Facebook
						$this->db->where('iduser', $usuario_row->iduser);
						$this->db->where('status', 1);
						$facebook = $this->db->get('facebook');
						
						//Consultamos si tiene conexión con Twitter
						$this->db->where('iduser', $usuario_row->iduser);
						$this->db->where('status', 1);
						$twitter = $this->db->get('twitter');
						
						//Declaramos 
						$usuarios = array();
						
						//Verificamos
						if ($facebook->num_rows() > 0 || $twitter->num_rows() > 0)
						{
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Declaramos el Arreglo Vacio
							$friends = array();
									
							//Caso FACEBOOK
							if ($facebook->num_rows() > 0)
							{
								//Leemos el Objeto
								$facebook_row = $facebook->row();
								
								//Consultamos los Amigos en Facebook
								$amigos = $this->curl->simple_get('https://graph.facebook.com/'.$facebook_row->facebook_id.'/friends?access_token='.$facebook_row->access_token_long.'&fields=name,picture&limit=5000&offset=0');
								$array_amigos = json_decode($amigos, true);
								
								//Revisamos
								if (isset($array_amigos['data']))
								{
									//Procesamos los Amigos
									foreach ($array_amigos['data'] as $amigo)
									{
										//Consultamos si es Usuario
										$this->db->where('facebook_id', (string)trim($amigo['id']));
										$this->db->where('status', (int)1);
										$query = $this->db->get('facebook');
										
										//Verificamos
										if ($query->num_rows() > 0)
										{
											//Leemos el Objeto
											$row = $query->row();
											
											//Consultamos si ya es Favorito
											$this->db->where('user', $user);
											$this->db->where('friend', $row->user);
											$es_amigo = $this->db->get('friendship');
											
											//Generamos el arreglo
											$friends[] = array(
												'network' => 'facebook',
												'network_name' => $amigo['name'],
												'network_id' => $amigo['id'],
												'network_avatar' => $amigo['picture']['data']['url'],
												'iduser' => $row->iduser,
												'user' => $row->user,
												'favorited' => ($es_amigo->num_rows() > 0) ? 1 : 0
											);
											
											//Usuarios
											$usuarios[] = $row->user;
										}
									}
								}
							}
							
							$error = 0;
							
							if ($twitter->num_rows() > 0)
							{
								//Leemos el Objeto
								$twitter_row = $twitter->row();
								
								//Consultamos los Amigos
								$amigos = $this->twitterapi->getFriends($twitter_row->screen_name, $twitter_row->oauth_token, $twitter_row->oauth_token_secret);
								$contador = 0;
								
								//Verificamos Error
								$error = (isset($amigos->errors[0]->message)) ? 1 : 0;
						
								//Revisamos
								if (isset($amigos->ids))
								{
									//Procesamos los Amigos
									foreach ($amigos->ids as $amigo)
									{
										//Consultamos si es Usuario
										$this->db->where('user_id', (string)trim($amigo));
										$this->db->where('status', (int)1);
										$query = $this->db->get('twitter');
										
										//Verificamos
										if ($query->num_rows() > 0)
										{
											//Leemos el Objeto
											$row = $query->row();
											
											//Declaramos
											$agregado = 0;
											
											//Verificamos
											foreach ($usuarios as $fb_user)
											{
												if ($fb_user==$row->user) { $agregado++; }
											}
											
											//Verificamos
											if ($agregado == 0)
											{
												//Consultamos si ya es Favorito
												$this->db->where('user', $user);
												$this->db->where('friend', $row->user);
												$es_amigo = $this->db->get('friendship');
												
												//Generamos el arreglo
												$friends[] = array(
													'network' => 'twitter',
													'network_name' => $row->screen_name,
													'network_id' => $row->user_id,
													'network_avatar' => $row->avatar,
													'iduser' => $row->iduser,
													'user' => $row->user,
													'favorited' => ($es_amigo->num_rows() > 0) ? 1 : 0
												);
											}
										}
									}
								}
							}
							
							//Nuevo Arreglo
							$query = array(
								'limit' => $error,
								'friends' => $friends
							);
							
							//Unimos el primer arreglo con el resultado
							$array += array(
								'data' => $query
							);
									
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This user did not has connected social networks.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This user does not exists or is disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// setFriend
			if ($msg == 'setFriend')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos las Variables
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					$friend = (isset($fields['friend'])) ? (string)trim($fields['friend']) : '';
					
					//Verificamos
					if ($user!='' && $friend!='')
					{
						//Consultamos que no exista
						$this->db->where('user', $user);
						$this->db->where('friend', $friend);
						$amigos = $this->db->get('friendship');
						
						//Verificamos
						if ($amigos->num_rows() == 0)
						{
							//Consultamos el Número de Amigos del Usuario
							$this->db->where('user', $user);
							$relaciones = $this->db->get('friendship');
							
							//Verificamos
							if ($relaciones->num_rows() < 20)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								$this->db->where('user', $user);
								$usuario = $this->db->get('user')->row();
								
								$this->db->where('user', $friend);
								$amigo = $this->db->get('user')->row();
								
								//Generamos la Relacion
								$data = array(
									'iduser' => $usuario->iduser,
									'user' => $user,
									'idfriend' => $amigo->iduser,
									'friend' => $friend
								);
								$this->db->insert('friendship', $data);
								
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This user has 20 or more friends.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'These users already friends.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'You must sent an user and a friend to use this call.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// unsetFriend
			if ($msg == 'unsetFriend')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos las Variables
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					$friend = (isset($fields['friend'])) ? (string)trim($fields['friend']) : '';
					
					//Verificamos
					if ($user!='' && $friend!='')
					{
						//Consultamos que no exista
						$this->db->where('user', $user);
						$this->db->where('friend', $friend);
						$amigos = $this->db->get('friendship');
						
						//Verificamos
						if ($amigos->num_rows() > 0)
						{
							//Leemos el Objeto
							$row = $amigos->row();
							
							//Generamos el Arreglo
							$array = array(
								'status' => (int)1,
								'msg' => 'success'
							);
							
							//Generamos la Relacion
							$this->db->where('idfriendship', $row->idfriendship);
							$this->db->delete('friendship');
							
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'These users are not friends.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'You must sent an user and a friend to use this call.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// disconnectNetwork
			if ($msg == 'disconnectNetwork')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el user
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Verificamos
					if ($user != '')
					{
						//Consultamos si existe el usuario
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$usuario = $this->db->get('user');
						
						//Verificamos
						if ($usuario->num_rows() > 0)
						{
							//Leemos el Objeto
							$usuario_row = $usuario->row();
							
							//Consultamos la Red Social
							$this->db->where('iduser', $usuario_row->iduser);
							$this->db->where('status', 1);
							$relacion = $this->db->get($fields['network']);
							
							//Verificamos
							if ($relacion->num_rows() > 0)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Cambiamos el Status
								$data = array(
									'status' => (int)0
								);
								$this->db->where('iduser', $usuario_row->iduser);
								$this->db->update($fields['network'], $data);
								
								//Consultamos todas las Relaciones que hizo con la Red Social
								$this->db->where('iduser', $usuario_row->iduser);
								$relaciones = $this->db->get('friendship');
								
								//Procesamos
								foreach ($relaciones->result() as $row)
								{
									//Consultamos si es de la Red Social
									$this->db->where('iduser', $row->idfriend);
									$network = $this->db->get($fields['network']);
									
									//Verificamos
									if ($network->num_rows() > 0)
									{
										//Eliminamos la Amistad
										$this->db->where('idfriendship', $row->idfriendship);
										$this->db->delete('friendship');
									}
								}
								
								//Network
								$network = array(
									'network' => $fields['network']
								);
								
								//Unimos el primer arreglo con el resultado
								$array += array(
									'data' => $network
								);
								
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'This uses does not have connected '.$fields['network'].' profile.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This uses does not exists or its disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'You must sent an user to use this call.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// ALBUMS
			// getAlbum
			if ($msg == 'getAlbum')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el OFFSET
					$offset = (isset($fields['offset'])) ? (int)$fields['offset'] : 0;
					
					//Consultamos el Album
					$this->db->where('status', 1);
					$this->db->where('timestamp >', '1378011600');
					$this->db->order_by('timestamp', 'DESC');
					$fotos = $this->db->get('photo', 20, $offset);
					
					//Verificamos
					if ($fotos->num_rows() > 0)
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Declaramos el Arreglo
						$photos = array();
						
						//Procesamos las Fotos
						foreach ($fotos->result() as $foto)
						{
							$handle = curl_init($foto->user_avatar);
							curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
							
							/* Get the HTML or whatever is linked in $url. */
							$response = curl_exec($handle);
							
							/* Check for 404 (file not found). */
							$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
							if($httpCode == 200) {
							    /* Handle 404 here. */
							    //Guardamos la Imagen
								$avatar = $foto->user_avatar;
							}
							else
							{
								//Mandamos el Avatar Default
								$avatar = 'http://192.241.204.161/api/unknown.png';
							}
							
							curl_close($handle);
							
							//Generamos el Arreglo
							$photos[] = array(
								'username' => $foto->user_name,
								'avatar' => $avatar,
								'type' => $foto->type,
								'source' => $foto->image,
								'text' => (string)trim($foto->user_text),
								'link' => (string)trim($foto->link),
								'timestamp' => $foto->timestamp,
								'timestamp_string' => $this->getTimestampString($foto->timestamp)
							);
						}
						
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $photos
						);
								
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no photos available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// DISCOVER
			// getDiscover
			if ($msg == 'getDiscover')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el user
					$user = (isset($fields['user'])) ? (string)trim($fields['user']) : '';
					
					//Verificamos
					if ($user != '')
					{
						//Consultamos el Usuario en BD
						$this->db->where('user', $user);
						$this->db->where('status', 1);
						$usuario = $this->db->get('user');
						
						//Verificamos
						if ($usuario->num_rows() > 0)
						{
							//Leemos el Objeto
							$usuario_row = $usuario->row();
							
							//Consultamos las Bandas Agendadas
							$this->db->where('iduser', $usuario_row->iduser);
							$agendadas = $this->db->get('scheduled');
							
							//Verificamos
							if ($agendadas->num_rows() > 0)
							{
								//Generamos el Arreglo
								$array = array(
									'status' => (int)1,
									'msg' => 'success'
								);
								
								//Declaramos el Arreglo
								$bands = array();
								
								//Procesamos las Bandas Agendadas
								foreach ($agendadas->result() as $agendada)
								{
									//Consultamos la Banda
									$this->db->where('idband', $agendada->idband);
									$this->db->where('status', 1);
									$banda = $this->db->get('band');
									
									//Verificamos
									if ($banda->num_rows() > 0)
									{
										//Leemos el Objeto
										$banda_row = $banda->row();
										
										//Consultamos los Tags de la Banda
										$this->db->where('idband', $banda_row->idband);
										$tags = $this->db->get('band_tag');
										
										//Verificamos
										if ($tags->num_rows() > 0)
										{
											//Leemos los Datos de la Banda
											$this->db->where('idband', $banda_row->idband);
											$info = $this->db->get('info_band')->row();
											
											//Declaramos el Arreglo de Tags
											$array_tags = array();
											
											//Procesamos los Tags
											foreach ($tags->result() as $tag)
											{
												//Leemos el Tag
												$this->db->where('idtag', $tag->idtag);
												$row = $this->db->get('tag')->row();
												
												//Generamos el Elemento
												$array_tags[] = array(
													'idtag' => $row->idtag,
													'tag' => $row->tag,
													'idband_tag' => $tag->idband_tag
												);
											}
											
											//Ordenamos por ID de Relacion
											$array_tags = $this->subval_sort($array_tags,'idband_tag');
											
											//Generamos el Contador de Bandas Relacionadas
											$relacionadas = 0;
											
											//Declaro Arreglo de Bandas Relacionadas
											$related = array();
											
											//Procesamos el Arreglo de Tags
											foreach ($array_tags as $tag)
											{
												//Consultamos los resultados con el tag de primer nivel
												$query = $this->db->query('SELECT relacion.idband, (SELECT name FROM band WHERE band.idband = relacion.idband) as name, (SELECT slug FROM band WHERE band.idband = relacion.idband) as slug, (SELECT discover_image FROM info_band WHERE info_band.idband = relacion.idband) as discover_image FROM band_tag relacion WHERE relacion.idtag = '.$tag['idtag'].' AND relacion.idband != '.$banda_row->idband.' AND relacion.value = 1 ORDER BY relacion.value ASC');
												
												//Procesamos
												foreach ($query->result() as $row)
												{
													//Aumentamos el Contador
													$relacionadas++;
													
													//Agreamos la Banda Relacionada
													$related[] = array(
														'idband' => $row->idband,
														'name' => $row->name,
														'featured_image' => $row->discover_image,
														'share' => $this->getShare($row->idband)
													);
													
													//Verificamos
													if ($relacionadas==3)
													{
														break;
													}
												}
												
												//Verificamos
												if ($relacionadas==3)
												{
													break;
												}
											}
											
											//Generamos el Arreglo
											$bands[] = array(
												'idband' => $banda_row->idband,
												'name' => $banda_row->name,
												'slug' => $banda_row->slug,
												'featured_image' => $info->featured_image,
												'share' => $this->getShare($banda_row->idband),
												'related' => $related
											);
										}
									}
								}
								
								//Unimos el primer arreglo con el resultado
								$array += array(
									'data' => $bands
								);
										
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
							else
							{
								//Respuesta de Error
								$array = array(
									'status' => (int)0,
									'msg' => (string)'There are no scheduled bands with this user.'
								);
									
								//Imprimimos el Arreglo
								$this->printJSON($array);
								$output = TRUE;
							}
						}
						else
						{
							//Respuesta de Error
							$array = array(
								'status' => (int)0,
								'msg' => (string)'This user does not exists or is disabled.'
							);
								
							//Imprimimos el Arreglo
							$this->printJSON($array);
							$output = TRUE;
						}
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This API call must have a user to work.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getDiscoverBand
			if ($msg == 'getDiscoverBand')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID Banda
					$idband = (isset($fields['idband'])) ? (int)$fields['idband'] : 0;
					$bandas_relacionadas = (isset($fields['bands'])) ? (string)trim($fields['bands']) : '';
					
					//Consultamos la Banda
					$this->db->where('idband', $idband);
					$this->db->where('status', 1);
					$banda = $this->db->get('band');
					
					//Verificamos
					if ($banda->num_rows() > 0)
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Leemos el Objeto
						$banda_row = $banda->row();
						
						//Consultamos los Tags de la Banda
						$this->db->where('idband', $banda_row->idband);
						$tags = $this->db->get('band_tag');
						
						//Verificamos
						if ($tags->num_rows() > 0)
						{
							//Leemos los Datos de la Banda
							$this->db->where('idband', $banda_row->idband);
							$info = $this->db->get('info_band')->row();
							
							//Declaramos el Arreglo de Tags
							$array_tags = array();
							
							//Procesamos los Tags
							foreach ($tags->result() as $tag)
							{
								//Leemos el Tag
								$this->db->where('idtag', $tag->idtag);
								$row = $this->db->get('tag')->row();
								
								//Generamos el Elemento
								$array_tags[] = array(
									'idtag' => $row->idtag,
									'tag' => $row->tag,
									'idband_tag' => $tag->idband_tag,
									'value' => $tag->value
								);
							}
							
							//Ordenamos por ID de Relacion
							$array_tags = $this->subval_sort($array_tags,'idband_tag');
							
							//Generamos el Contador de Bandas Relacionadas
							$relacionadas = 0;
							
							//Declaro Arreglo de Bandas Relacionadas
							$related = array();
							
							//Procesamos las Bandas
							if ($bandas_relacionadas != '')
							{
								$bandas_relacionadas = $bandas_relacionadas.','.$banda_row->idband;
							}
							else
							{
								$bandas_relacionadas = $banda_row->idband;
							}
						
							//Procesamos el Arreglo de Tags
							foreach ($array_tags as $tag)
							{
								//Construimos la Consulta
								$str_query = 'SELECT relacion.idband, (SELECT name FROM band WHERE band.idband = relacion.idband) as name, (SELECT slug FROM band WHERE band.idband = relacion.idband) as slug, (SELECT discover_image FROM info_band WHERE info_band.idband = relacion.idband) as discover_image FROM band_tag relacion WHERE relacion.idtag = ';
								$str_query.= $tag['idtag'];
								$str_query.= ' AND relacion.idband NOT IN (';
								$str_query.= $bandas_relacionadas;
								$str_query.= ') AND relacion.value = ';
								$str_query.= $tag['value'];
								$str_query.= ' ORDER BY relacion.value ASC';
								
								//Consultamos los resultados con el tag de primer nivel
								$query = $this->db->query($str_query);
								
								//Procesamos
								foreach ($query->result() as $row)
								{
									//Aumentamos el Contador
									$relacionadas++;
									
									//Agreamos la Banda Relacionada
									$related[] = array(
										'idband' => $row->idband,
										'name' => $row->name,
										'featured_image' => $row->discover_image,
										'share' => $this->getShare($row->idband)
									);
									
									$bandas_relacionadas = $bandas_relacionadas.','.$row->idband;
									
									//Verificamos
									if ($relacionadas==3)
									{
										break;
									}
								}
								
								//Verificamos
								if ($relacionadas==3)
								{
									break;
								}
							}
							
							//Generamos el Arreglo
							$band = array(
								'idband' => $banda_row->idband,
								'name' => $banda_row->name,
								'featured_image' => $info->featured_image,
								'share' => $this->getShare($banda_row->idband),
								'related' => $related
							);
						}
						
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $band
						);
								
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
						
						
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This band does not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			/* BACKEND */
			
			// getAllMedia
			if ($msg == 'getAllMedia')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos la Variable
					$offset = (isset($fields['offset'])) ? (int)$fields['offset'] : 0;
					
					//Consultamos las Fotos
					$this->db->where('status', (int)0);
					$this->db->where('timestamp >', '1378011600');
					$this->db->order_by('timestamp', 'desc');
					$fotos = $this->db->get('photo', 20, $offset);
					
					$this->db->where('status', (int)0);
					$this->db->where('timestamp >', '1378011600');
					$this->db->order_by('timestamp', 'desc');
					$todas = $this->db->get('photo');
					
					//Verificamos
					if ($fotos->num_rows() > 0)
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'total' => $todas->num_rows()
						);
						
						//Declaramos el Arreglo Vacío
						$photos = array();
						
						//Procesamos las Fotos
						foreach ($fotos->result() as $foto)
						{
							$handle = curl_init($foto->user_avatar);
							curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
							
							/* Get the HTML or whatever is linked in $url. */
							$response = curl_exec($handle);
							
							/* Check for 404 (file not found). */
							$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
							if($httpCode == 200) {
							    /* Handle 404 here. */
							    //Guardamos la Imagen
								$avatar = $foto->user_avatar;
							}
							else
							{
								//Mandamos el Avatar Default
								$avatar = 'http://192.241.204.161/api/unknown.png';
							}
							
							curl_close($handle);
							
							//Generamos los Elementos
							$photos[] = array(
								'idphoto' => $foto->idphoto,
								'user' => array(
									'avatar' => $avatar,
									'name' => $foto->user_name
								),
								'text' => $foto->user_text,
								'src' => $foto->image,
								'timestamp' => $foto->timestamp,
								'timestamp_string' => $this->getTimestampString($foto->timestamp),
								'link' => $foto->link,
								'type' => $foto->type,
								'status' => $foto->status
							);
						}
						
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $photos
						);
								
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no media available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getAllMediaApproved
			if ($msg == 'getAllMediaApproved')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos la Variable
					$offset = (isset($fields['offset'])) ? (int)$fields['offset'] : 0;
					
					//Consultamos las Fotos
					$this->db->where('status', (int)1);
					$this->db->where('timestamp >', '1378011600');
					$this->db->order_by('timestamp', 'desc');
					$fotos = $this->db->get('photo', 20, $offset);
					
					$this->db->where('status', (int)1);
					$this->db->where('timestamp >', '1378011600');
					$this->db->order_by('timestamp', 'desc');
					$todas = $this->db->get('photo');
					
					//Verificamos
					if ($fotos->num_rows() > 0)
					{
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success',
							'total' => $todas->num_rows()
						);
						
						//Declaramos el Arreglo Vacío
						$photos = array();
						
						//Procesamos las Fotos
						foreach ($fotos->result() as $foto)
						{
							$handle = curl_init($foto->user_avatar);
							curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
							
							/* Get the HTML or whatever is linked in $url. */
							$response = curl_exec($handle);
							
							/* Check for 404 (file not found). */
							$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
							if($httpCode == 200) {
							    /* Handle 404 here. */
							    //Guardamos la Imagen
								$avatar = $foto->user_avatar;
							}
							else
							{
								//Mandamos el Avatar Default
								$avatar = 'http://192.241.204.161/api/unknown.png';
							}
							
							curl_close($handle);
							
							//Generamos los Elementos
							$photos[] = array(
								'idphoto' => $foto->idphoto,
								'user' => array(
									'avatar' => $avatar,
									'name' => $foto->user_name
								),
								'text' => $foto->user_text,
								'src' => $foto->image,
								'timestamp' => $foto->timestamp,
								'timestamp_string' => $this->getTimestampString($foto->timestamp),
								'link' => $foto->link,
								'type' => $foto->type,
								'status' => $foto->status
							);
						}
						
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $photos
						);
								
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no media available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// deleteMedia
			if ($msg == 'deleteMedia')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID
					$idphoto = (isset($fields['idphoto'])) ? (int)$fields['idphoto'] : 0;
					
					//Consultamos
					$this->db->where('idphoto', $idphoto);
					$this->db->where('status <=', 1);
					$foto = $this->db->get('photo');
					
					//Verificamos
					if ($foto->num_rows() > 0)
					{
						//Leemos el Objeto
						$foto_row = $foto->row();
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Actualizamos su status
						$data = array(
							'status' => 2
						);
						$this->db->where('user_name', $foto_row->user_name);
						$this->db->where('timestamp', $foto_row->timestamp);
						$this->db->update('photo', $data);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no media available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// addMedia
			if ($msg == 'addMedia')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID
					$idphoto = (isset($fields['idphoto'])) ? (int)$fields['idphoto'] : 0;
					
					//Consultamos
					$this->db->where('idphoto', $idphoto);
					$this->db->where('status <=', 1);
					$foto = $this->db->get('photo');
					
					//Verificamos
					if ($foto->num_rows() > 0)
					{
						//Leemos el Objeto
						$foto_row = $foto->row();
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Actualizamos su status
						$data = array(
							'status' => 1
						);
						$this->db->where('idphoto', $foto_row->idphoto);
						$this->db->update('photo', $data);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no media available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// getAllMessages
			if ($msg == 'getAllMessages')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el id band
					$offset = (isset($fields['offset'])) ? (int)$fields['offset'] : 0;
					
					//Consultamos los Mensajes
					$this->db->order_by('idwall_band', 'desc');
					$mensajes  = $this->db->get('wall_band', 50, $offset);
					
					//Verificamos
					if ($mensajes->num_rows() > 0)
					{
						//Declaramos el Arreglo de Mensajes
						$messages = array();
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Contador
						$contador = 0;
						
						//Procesamos los Mensajes
						foreach ($mensajes->result() as $mensaje)
						{
							//Consultamos el Usuario
							$this->db->where('iduser', $mensaje->iduser);
							$this->db->where('status', 1);
							$usuario = $this->db->get('user');
							
							//Verificamos 
							if ($usuario->num_rows() > 0)
							{
								//Declaramos el Arreglo de Usuario
								$user = array();
								
								//Usuario Facebook
								$this->db->where('iduser', $mensaje->iduser);
								$this->db->where('status', 1);
								$facebook = $this->db->get('facebook');
								
								//Verificamos
								if ($facebook->num_rows() > 0)
								{
									//Leemos el Objeto
									$facebook_row = $facebook->row();
									
									//Generamos el Usuario
									$user = array(
										'user' => $facebook_row->user,
										'name' => $facebook_row->first_name,
										'avatar' => 'https://graph.facebook.com/'.$facebook_row->facebook_id.'/picture'
									);
								}
								else
								{
									//Usuario Twitter
									$this->db->where('iduser', $mensaje->iduser);
									$this->db->where('status', 1);
									$twitter = $this->db->get('twitter');
									
									//Verificamos
									if ($twitter->num_rows() > 0)
									{
										//Leemos el Objeto
										$twitter_row = $twitter->row();
										
										//Generamos el Usuario
										$user = array(
											'user' => $twitter_row->user,
											'name' => $twitter_row->name,
											'avatar' => $twitter_row->avatar
										);
									}
								}
								
								//Verificamos que tenga usuario conectado
								if (count($user) > 0)
								{		
									//Incrementamos el Contador
									$contador++;
										
									//Leemos la Banda
									$this->db->where('idband', $mensaje->idband);
									$banda = $this->db->get('band')->row();
									
									//Revisamos si esta Reportado por el usuario
									$this->db->where('idwall_band', $mensaje->idwall_band);
									$this->db->where('idband', $banda->idband);
									$reportado = $this->db->get('report_wall_band');
																									
									//Generamos el Elemento
									$messages[] = array(
										'idwall_band' => (int)$mensaje->idwall_band,
										'text' => (string)trim($mensaje->text),
										'timestamp' => (string)trim($mensaje->timestamp),
										'timestamp_string' => $this->getTimestampString($mensaje->timestamp),
										'date' => (string)trim($mensaje->date),
										'user' => $user,
										'band' => array(
											'idband' => $banda->idband,
											'name' => $banda->name,
											'slug' => $banda->slug,
											'link' => $banda->link
										),
										'status' => $mensaje->status,
										'reported' => ($reportado->num_rows() > 0) ? 1 : 0
									);
								}
							}
							
							//Rompemos
							if ($contador == 20)
							{
								//Declaramos la Respuesta
								$pages = array(
									'messages' => $messages,
									'next' => ($mensajes->num_rows() > $contador) ? '1' : '0'
								);
								break;
							}
						}
						
						//Unimos el primer arreglo con el resultado
						$array += array(
							'data' => $pages
						);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'This band does not exists or its disabled.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// deleteMessage
			if ($msg == 'deleteMessage')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID
					$idwall_band = (isset($fields['idwall_band'])) ? (int)$fields['idwall_band'] : 0;
					
					//Consultamos
					$this->db->where('idwall_band', $idwall_band);
					$mensaje = $this->db->get('wall_band');
					
					//Verificamos
					if ($mensaje->num_rows() > 0)
					{
						//Leemos el Objeto
						$mensaje_row = $mensaje->row();
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Actualizamos su status
						$data = array(
							'status' => 0
						);
						$this->db->where('idwall_band', $idwall_band);
						$this->db->update('wall_band', $data);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no message available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			// addMessage
			if ($msg == 'addMessage')
			{
				//Verificamos que las credenciales sean correctas
				if ($app == $app_row->app && $apikey == $app_row->apikey)
				{
					//Leemos el ID
					$idwall_band = (isset($fields['idwall_band'])) ? (int)$fields['idwall_band'] : 0;
					
					//Consultamos
					$this->db->where('idwall_band', $idwall_band);
					$mensaje = $this->db->get('wall_band');
					
					//Verificamos
					if ($mensaje->num_rows() > 0)
					{
						//Leemos el Objeto
						$mensaje_row = $mensaje->row();
						
						//Generamos el Arreglo
						$array = array(
							'status' => (int)1,
							'msg' => 'success'
						);
						
						//Actualizamos su status
						$data = array(
							'status' => 1
						);
						$this->db->where('idwall_band', $idwall_band);
						$this->db->update('wall_band', $data);
						
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
					else
					{
						//Respuesta de Error
						$array = array(
							'status' => (int)0,
							'msg' => (string)'There are no message available.'
						);
							
						//Imprimimos el Arreglo
						$this->printJSON($array);
						$output = TRUE;
					}
				}
			}
			
			//Verificamos la Salida de Informacion
			if (!$output) 
			{
				$array = array(
					'status' => (int)0,
					'msg' => (string)'api call invalid'
				);
				
				//Imprimimos el Arreglo
				$this->printJSON($array);
			}
		}
		else
		{
			$array = array(
				'status' => (int)0,
				'msg' => (string)'invalid app'
			);
			
			//Imprimimos el Arreglo
			$this->printJSON($array);
		}
		
		//Generamos el Log
		$this->api_log($app,$msg,$array['status'],$array['msg']);
	}
	
	public function getShare($idband)
	{
		//Consultamos 
		$this->db->order_by('idshare', 'random');
		$compartir = $this->db->get('share',1)->row();
		
		//Consultamos Banda
		$this->db->where('idband', $idband);
		$banda = $this->db->get('band')->row();
		
		//Cambiamos los Campos
		$share = str_replace('@banda', $banda->name, $compartir->text);
		$share = str_replace('@link', $banda->link, $share);
		
		//Regresamos el Valor
		return $share;
	}
	
	public function getNewID()
	{
		$random = random_string('alnum', 8);
		$user = sha1($random);
		return $user;
	}
	
	public function subval_sort($toOrderArray, $field, $inverse = false) 
	{
	    $position = array();
	    $newRow = array();
	    foreach ($toOrderArray as $key => $row) {
	            $position[$key]  = $row[$field];
	            $newRow[$key] = $row;
	    }
	    if ($inverse) {
	        arsort($position);
	    }
	    else {
	        asort($position);
	    }
	    $returnArray = array();
	    foreach ($position as $key => $pos) {     
	        $returnArray[] = $newRow[$key];
	    }
	    return $returnArray;
	}
	
	public function elimina_duplicados($array, $campo)
	{
	  foreach ($array as $sub)
	  {
	    $cmp[] = $sub[$campo];
	  }
	  $unique = array_unique($cmp);
	  foreach ($unique as $k => $campo)
	  {
	    $resultado[] = $array[$k];
	  }
	  return $resultado;
	}
	
	public function api_log($app,$call,$status,$msg)
	{
		//Revisamos si la llamada no es el Log de la APP
		if ((string)trim($call) != 'appLog')
		{
			//Generamos el Registro del Log
			$data = array(
				'app' => (string)trim($app),
				'call' => (string)trim($call),
				'status' => (int)$status,
				'msg' => (string)trim($msg),
				'timestamp' => now()
			);
			$this->db->insert('api_log', $data);
		}
	}
	
	public function getProfile($idprofile=0)
	{
		//Verificamos
		if ($idprofile != 0)
		{
			//Consultamos Perfil
			$this->db->where('idprofile', $idprofile);
			$perfil = $this->db->get('profile');
			
			//Verificamos
			if ($perfil->num_rows() > 0)
			{
				//Leemos el Objeto
				$perfil_row = $perfil->row();
				
				//Regresamos el Arreglo
				return $perfil_row;	
			}
			else
			{
				//Regresamos un Arreglo Vacio
				return array();
			}
		}
		else
		{
			//Regresamos un Arreglo Vacio
			return array();
		}
	}
	
	public function getTimestampString($session_time)
	{
		$time_difference = time() - $session_time ; 
		
		$seconds = $time_difference ; 
		$minutes = round($time_difference / 60 );
		$hours = round($time_difference / 3600 ); 
		$days = round($time_difference / 86400 ); 
		$weeks = round($time_difference / 604800 ); 
		$months = round($time_difference / 2419200 ); 
		$years = round($time_difference / 29030400 ); 
		
		$string = '';
		
		// Seconds
		if($seconds <= 60) { $string = "$seconds seconds ago"; }
		//Minutes
		else if($minutes <=60)
		{ 
			if($minutes==1) { $string = "hace un minuto"; }
			else { $string = "hace $minutes minutos"; }
		}
		//Hours
		else if($hours <=24)
		{	
			if($hours==1) { $string = "hace una hora"; }
			else { $string = "hace $hours horas"; }
		}
		//Days
		else if($days <= 7)
		{
		  	if($days==1) { $string = "hace un día"; }
		  	else { $string = "hace $days días"; }
		}
		//Weeks
		else if($weeks <= 4)
		{
			if($weeks==1) { $string = "hace una semana"; }
			else { $string = "hace $weeks semanas"; }
		}
		//Months
		else if($months <=12)
		{
			if($months==1){ $string = "hace un mes"; }
			else { $string = "hace $months meses"; }
		}
		//Years
		else
		{
			if($years==1) { $string = "hace un año"; }
			else { $string = "hace $years años"; }
		}
		
		//Return String
		return $string;
	}

}